const bluebird = require('bluebird')
const _        = require('lodash')

const postgres = require('../../../src/common/postgres/tables.js')
const SPClient = require('../../clients/SPClient.js')
const { generate_user_data } = require('../../utils/seed_data.js')
const chai     = require('chai');
chai.use(require('chai-exclude'));
const assert = chai.assert

describe(`reports`, function(){

    let clients ;
    let admin ; 
    let group_id ;
    let report_object ;

    before(async function(){
    
        // Get 2 regular users
        clients = await bluebird.map( _.range(2), async () => {
            let cli = new SPClient('localhost:8000')
            const res = await cli.reg_and_verify({
                body: _.merge( {tos:true}, generate_user_data() ) 
            })
            cli.id = await cli.get_my_id()
            return cli
        })

        // Get an admin user
        admin = new SPClient('localhost:8000')
        const res = await admin.register({
            body: _.merge( {tos:true}, generate_user_data() ) 
        })
        admin.authenticate(res.body.token)
        await admin.set_admin()

        report_object = {
            reason: "This guy is an asshole",
            other_user_id: clients[1].id
        }
        assert.isNotNull(clients[1].id, "This test requres the id to not be fucked")
    })

    it (`User 1 should be able to report User 2`, async function(){
        const response = await clients[0].report({ body: report_object })
        const report = response.body
        assert.equal(report.user_id, clients[0].id, "reporting user id should correct")
        assert.equal(report.other_user_id, report_object.other_user_id, "reported user id should correct")
        assert.equal(report.group_id, null, "No group should be provided")
        assert.equal(report.reason, report_object.reason, "reaason")
    })

    it (`Admin user should be able to see report`, async function(){
        const response = await admin.view_reports({
            query_strings: { user_id: clients[0].id }
        })
        assert.equal(response.body.length, 1, "should see 1 report")
        const report = response.body[0]
        assert.equal(report.user_id, clients[0].id, "reporting user id should correct")
        assert.equal(report.other_user_id, report_object.other_user_id, "reported user id should correct")
        assert.equal(report.group_id, null, "No group should be provided")
        assert.equal(report.reason, report_object.reason, "reaason")
    })

})