const bluebird = require('bluebird')
const _        = require('lodash')

const postgres = require('../../../src/common/postgres/tables.js')
const SPClient = require('../../clients/SPClient.js')
const { generate_user_data } = require('../../utils/seed_data.js')
const chai     = require('chai');
chai.use(require('chai-exclude'));
const assert = chai.assert

// See general_preferences schema
const prefs = {
    number_of_roommates  : 1,
    schedule             : 'night_owl' ,
    noise_level          : 'social' ,
    gender_preference    : 'coed' ,
    age_restriction      : false  ,
    lgbtq_friendly       : true   ,
} 

const room_prefs = {
    check_in_date        : new Date('3000-01-01').toISOString() ,
    check_out_date       : new Date('3000-01-04').toISOString() ,
    max_budget           : 200,
    other_details        : "jews, eskimos and lesbians preferred"
} 

const room = {
    check_in_date                 : new Date('3000-01-01').toISOString() ,
    check_out_date                : new Date('3000-01-04').toISOString() ,
    price_total                   : 800 ,
    accommodation_name            : 'Some Hotel' ,
    address_line_1                : '123 Address street' ,
    address_line_2                : 'Room 1408' ,
    zipcode                       : '12345' ,
    city                          : 'San Francisco' ,
    state                         : 'CA' ,
    other_details                 : "No jews, eskimos and lesbians allowed"
}

const updated_room_fields = {
    accommodation_name            : 'Other Hotel' ,
    address_line_1                : '321 Address avenue' ,
    other_details                 : "Actually, eskimos are OK."
}


const request_no_room = { event_id: 1, prefs, room_prefs }

const request_with_room = { event_id: 1, prefs, room }


// JKHFLKUHJKLJDHS
const ignored_fields = ['id', 'created_on', 'updated_on', 'check_in_date', 'check_out_date']

describe(`requests`, function(){

    let clients ;
    let admin ;   
    let request_ids = [] ;
    let group_id ;
    let room_id ;

    before(async function(){

        // Register new users
        clients = await bluebird.map( _.range(4), async () => {
            let cli = new SPClient('localhost:8000')
            const res = await cli.reg_and_verify({
                body: _.merge( {tos:true}, generate_user_data() ) 
            })

            return cli
        })

        await clients[2].set_admin()        

        admin = clients[2]
    })
    
    const validate_request_no_room = function(request){
        assert.deepEqualExcluding(request.prefs, prefs, ignored_fields)
        assert.deepEqualExcluding(request.room_prefs, room_prefs, ignored_fields)
        assert.isNull(request.room,  "This request should not have a room")
        assert.equal( new Date(request.room_prefs.check_in_date).toISOString(), new Date(room_prefs.check_in_date).toISOString() )
        assert.equal( new Date(request.room_prefs.check_out_date).toISOString(), new Date(room_prefs.check_out_date).toISOString() )
    }

    const validate_request_with_room = function(request){
        assert.deepEqualExcluding(request.prefs, prefs, ignored_fields)
        assert.deepEqualExcluding(request.room, room, ignored_fields)
        assert.isNull(request.room_prefs,  "This request should not have room prefs")
        assert.equal( new Date(request.room.check_in_date).toISOString(), new Date(room.check_in_date).toISOString() )
        assert.equal( new Date(request.room.check_out_date).toISOString(), new Date(room.check_out_date).toISOString() )
    }

    const validate_group = function(group){
        assert.isNotNull(group, "grouop should not be null now ")
        assert.deepEqualExcluding(group.room, room, ignored_fields)
        // assert.equal(group.members.length, 2, "group should have 2 members")
        assert.equal( new Date(group.room.check_in_date).valueOf(), new Date(room.check_in_date).valueOf() )
        assert.equal( new Date(group.room.check_out_date).valueOf(), new Date(room.check_out_date).valueOf() )
    }
    
    it(`user 0 creates a request with no room`, async function(){
        const res = await clients[0].new_request({ body:  request_no_room })
        const request = res.body 
        // Requests are split into arrays for matched and unmatched
        request_ids.push( request.id )
        assert.equal(res.statusCode, 200, "Should return 200")
        validate_request_no_room(request)
        assert.isNull(request.group, "This request should not have a group")
    })
    
    it(`user 1 creates a request with a room`, async function(){
        const res = await clients[1].new_request({ body:  request_with_room })
        const request = res.body 
        // Requests are split into arrays for matched and unmatched
        request_ids.push( request.id )
        assert.equal(res.statusCode, 200, "Should return 200")
        validate_request_with_room(request)
        assert.isNull(request.group, "This request should not have a group")
    })

    it(`user 0 views requests`, async function(){
        const res = await clients[0].my_requests()
        const request_arrays = res.body 
        // Requests are split into arrays for matched and unmatched
        assert.equal(request_arrays.matched.length, 0)
        assert.equal(request_arrays.unmatched.length, 1)
        const request = request_arrays.unmatched[0]
        validate_request_no_room(request)
        assert.isNull(request.group, "This request should not have a group")
    })
    
    it(`user 1 views requests`, async function(){
        const res = await clients[1].my_requests()
        const request_arrays = res.body 
        // Requests are split into arrays for matched and unmatched
        assert.equal(request_arrays.matched.length, 0)
        assert.equal(request_arrays.unmatched.length, 1)
        const request = request_arrays.unmatched[0]
        validate_request_with_room(request)
        assert.isNull(request.group, "This request should not have a group")
        room_id = request.room.id
    })

    it(`admin user matches users 0 and 1 into a group`, async function(){
        await admin.make_group({ body: { request_ids } }) ;
    })

    it(`user 0 should see a matched group`, async function(){
        const res = await clients[0].my_requests()
        const request_arrays = res.body 
        // Requests are split into arrays for matched and unmatched
        assert.equal(request_arrays.unmatched.length, 0)
        assert.equal(request_arrays.matched.length, 1)
        const request = request_arrays.matched[0]
        validate_request_no_room(request)
        validate_group(request.group)
        group_id = request.group.id
    })
    
    it(`user 1 should see a matched group`, async function(){
        const res = await clients[1].my_requests()
        const request_arrays = res.body 
        // Requests are split into arrays for matched and unmatched
        assert.equal(request_arrays.unmatched.length, 0)
        assert.equal(request_arrays.matched.length, 1)
        const request = request_arrays.matched[0]
        validate_request_with_room(request)
        validate_group(request.group)
    })

    it(`group details response`, async function(){
        const res = await clients[1].get_group(group_id)
        const group = res.body
        validate_group(group)
        // console.log(group)
        assert.equal(group.members.length, 2, "group should have 2 members")
        group.members.forEach( m => {
            assert.ok(m.is_leader != null, "should have field is_leader")    
            assert.ok(m.member, "should have field member")    
            assert.ok(m.prefs, "should have field prefs")
        })
    })

    it(`leader updates room`, async function(){
        const res = await clients[1].update_room({ body: {id: room_id, ...updated_room_fields } }, {room_id})
        const new_room = res.body
        assert.equal(res.statusCode, 200)
        const updated_keys = _.keys(updated_room_fields)
        // assert.deepEqualExcluding(new_room, room, _.concat(ignored_fields, updated_keys))
        updated_keys.forEach(k => [
            assert.ok(new_room[k] === updated_room_fields[k], `Fields ${k} should match... ${new_room[k]} !== ${room[k]}`)
        ])

    })

    // it(`User 0 leaves group`, async function(){
    //     const res = await clients[0].my_requests()
    //     const requests = res.body.matched
    //     await bluebird.map(requests, async r => {
    //         const res = await clients[0].cancel_request({},{request_id: r.id})
    //         assert.equal(res.statusCode, 200)
    //     })
    // })

    it(`User 0 kicked from group`, async function(){
        const res = await clients[1].kick_member(
            group_id, 
            await clients[0].get_my_id() 
        )
        assert.equal(res.statusCode, 200)
    })

    it(`user 0 views requests`, async function(){
        const res = await clients[0].my_requests()
        assert.equal(res.body.matched.length, 0, "should have 0 requests")
    })

    it(`user 1 views requests`, async function(){
        const res = await clients[1].my_requests()
        assert.equal(res.body.matched.length, 0, "should have 0 requests")
    })

    it(`user 1 looks at disbanded group (expect 403)`, async function(){
        const res = await clients[1].get_group(group_id)
        assert.equal(res.statusCode, 403, "Status should have been 403")
    })

    
})