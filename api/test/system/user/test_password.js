const bluebird = require('bluebird')
const _        = require('lodash')

const postgres = require('../../../src/common/postgres/tables.js')
const SPClient = require('../../clients/SPClient.js')
const { generate_user_data } = require('../../utils/seed_data.js')
const chai     = require('chai');
chai.use(require('chai-exclude'));
const assert = chai.assert


describe(`password gymnastics`, function(){

    let cli1 ;
    let cli2 ;

    let data1, new_data1 ;
    let data2 ;

    let code ;

    before(async function(){

        cli1 = new SPClient('localhost:8000')
        cli2 = new SPClient('localhost:8000')

        data1 = generate_user_data()
        data2 = generate_user_data()
        new_data1 = generate_user_data()

        await cli1.register({ body: _.merge( {tos:true}, data1 ) })
        await cli2.register({ body: _.merge( {tos:true}, data2 ) })
        
    })

    it(`User forgets password [Sends code to email]`, async function(){
       const res = await cli1.forgot_password({body: {email: data1.email} })
       assert.equal(res.statusCode, 200)
    })

    it(`Get forgot-password code from email`, async function(){
        code = await cli1.get_code({
            op    : 'reset_password',
            email : data1.email
        })
        assert.ok(code)  
    })

    it(`confirm password change`, async function(){

        const res = await cli1.confirm_password_change({
            body: {
                code,
                password: "fuck you"
            }
        })

        assert.equal(res.statusCode, 200)

    })

    it(`user can log in with new password`, async function(){

        const res = await cli1.login({
            body: {
                email: data1.email,
                password: "fuck you"
            }
        })
        assert.ok(res.body.token)
        assert.equal(res.statusCode, 200)

    })

})