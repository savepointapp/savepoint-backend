const SPClient = require('../../clients/SPClient.js')
const assert = require('assert')
// const server   = require('../../../src/api/index.js')

/*  Hopefully you don't have to run this test often because its a pain in the ass.

    If you want to run this test, you must 
        (1) Get an access token for a test facebook user for savepoint
        (2) Run ". rebuild.sh" (from the top level of this repo) to destroy
            and rebuild your databanse.
*/

// This is fucking bullshit
// Find a way to fetch this programmatically 
// Also find a way to start the server before each test and tear it down after 
// Also find a way to setup and teardown database every time before tests
const FB_TOKEN = 'EAAVuwYioZC7MBAPkvzMY4ZADKwd35vITvEkDOoQESXT2BnhdBDvAE3A5KK5qsZCKPO9JIvkYOECSjhEAgjex64pOqyWRZBbk1Pd2BhtA9IBGi5is8BIepCkT2lhiZCIquIZC8aaxZBpByFJpQjFtXMxOo4OsSwMPTOtrIhlb5PyiiROZCToNFTRGd5lNvrE3uPQq2TtqrUx2DUo3lZCaH7ZCm54KZBZC4kuHTGGUlystS0mKPgZDZD'

describe.skip(`facebook signup tests`, function(){

    let cli, cli2 ;

    before(async function(){
        cli  = new SPClient('localhost:8000')
        cli2 = new SPClient('localhost:8000')
    })

    it(`POST /api/v1/register (facebook)`, async function(){
        const res = await cli.register({
            body: {
                token: FB_TOKEN
            }
        })
        assert.equal(res.statusCode, 200)
        assert.ok(res.body.token, "Response should have a savepoint token")
        cli.authenticate(res.body.token)
    })

    it(`GET /api/v1/profile`, async function(){
        const res = await cli.get_profile({})
        assert.equal(res.body.username          , null)
        assert.equal(res.body.email             , null)
        assert.equal(res.body.bio               , null) 
        assert.equal(res.body.profile_pic_url   , null)
        assert.equal(res.body.is_email_verified , false)
        assert.equal(res.body.encrypted_password, undefined)
    })

    it(`PUT /api/v1/profile`, async function(){
        const res = await cli.update_profile({
            body: {
                bio: "I'm a little teapot",
                over18: true,
                gender: "male"
            }
        })
        assert.equal(res.body.username          , null)
        assert.equal(res.body.email             , null)
        assert.equal(res.body.bio               , 'I\'m a little teapot') 
        assert.equal(res.body.gender            , 'male')
        assert.equal(res.body.over18            , true)
        assert.equal(res.body.profile_pic_url   , null)
        assert.equal(res.body.is_email_verified , false)
        assert.equal(res.body.encrypted_password, undefined)
    })

    it(`POST /api/v1/register (duplicate facebook signup)`, async function(){
        const res = await cli.register({
            body: {
                token: FB_TOKEN
            }
        })
        assert.equal(res.statusCode, 400)
    })

    it(`POST /api/v1/login (basic login)`, async function(){
        const res1 = await cli2.login({
            body: {
                token: FB_TOKEN
            }
        })
        assert.ok(res1.body.token)
        cli2.authenticate(res1.body.token)
        const res = await cli2.get_profile({})
        assert.equal(res.body.username          , null)
        assert.equal(res.body.email             , null)
        assert.equal(res.body.bio               , 'I\'m a little teapot') 
        assert.equal(res.body.gender            , 'male')
        assert.equal(res.body.over18            , true)
        assert.equal(res.body.profile_pic_url   , null)
        assert.equal(res.body.is_email_verified , false)
        assert.equal(res.body.encrypted_password, undefined)
    })
    

})