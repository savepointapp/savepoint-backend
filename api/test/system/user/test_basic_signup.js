const SPClient = require('../../clients/SPClient.js')
const assert   = require('assert')
const _        = require('lodash')
const generate_data = require('../../utils/seed_data.js').generate_user_data

const init_profile = {
    bio: 'Hello',
    first_name: 'Joe'  ,
    last_name: 'Biden'   ,
    email_sub: false   ,
    age: 100          ,
    tos: true         ,
    gender: 'male'   
}

const updated_profile = {
    bio: 'fuck u '       ,
    first_name: 'Barack' ,
    last_name: 'Obama'   ,
    email_sub: true      ,
    age: 101             ,
    tos: false           ,
    gender: 'other'
}

const fields = [ 'bio', 'first_name', 'last_name', 'email_sub', 'age', 'tos', 'gender' ]

describe(`basic signup`, function(){

    let cli, cli2 ;

    const reg_data = generate_data()

    before(async function(){
        cli  = new SPClient('localhost:8000')
        cli2 = new SPClient('localhost:8000')
    })

    it(`POST /api/v1/register`, async function(){
        const res = await cli.register({
            body: _.merge( reg_data, init_profile)
        })
        assert.equal(res.statusCode, 200)
        assert.ok(res.body.token, "Response should have a savepoint token")
        cli.authenticate(res.body.token)
    })

    it(`GET /api/v1/profile`, async function(){
        const res = await cli.get_profile({})
        assert.deepEqual(_.pick(res.body, fields), init_profile)
    })

    it(`PUT /api/v1/profile`, async function(){
        const res = await cli.update_profile({
            body: updated_profile
        })
        assert.deepEqual(_.pick(res.body, fields), updated_profile)
    })

    it(`POST /api/v1/register (duplicate)`, async function(){
        const res = await cli.register({
            body: _.merge(reg_data, init_profile)
        })
        assert.equal(res.statusCode, 400)
    })

    it(`POST /api/v1/login (basic login)`, async function(){
        const res1 = await cli2.login({
            body: reg_data
        })
        assert.ok(res1.body.token)
        cli2.authenticate(res1.body.token)
        const res = await cli2.get_profile({})
        assert.deepEqual(_.pick(res.body, fields), updated_profile)
    })
    

})