const bluebird = require('bluebird')
const _        = require('lodash')

const postgres = require('../../../src/common/postgres/tables.js')
const SPClient = require('../../clients/SPClient.js')
const { generate_user_data } = require('../../utils/seed_data.js')
const chai     = require('chai');
chai.use(require('chai-exclude'));
const assert = chai.assert


describe(`email verification`, function(){

    let cli1 ;
    let cli2 ;

    let data1, new_data1 ;
    let data2 ;

    before(async function(){

        cli1 = new SPClient('localhost:8000')
        cli2 = new SPClient('localhost:8000')

        data1 = generate_user_data()
        data2 = generate_user_data()
        new_data1 = generate_user_data()

        await cli1.register({ body: _.merge( {tos:true}, data1 ) })
        await cli2.register({ body: _.merge( {tos:true}, data2 ) })
        
    })

    // ----------------------------------------------
    //    On Register: (before email verification)
    // ----------------------------------------------

    it(`Before email verification: Login should work`, async function(){
        const res = await cli1.login({ body: data1 })
        assert.isNotNull(res.body.token, 'Should return savepoint token')
        assert.equal(res.statusCode, 200, "login should 200")
        cli1.authenticate(res.body.token)
    })

    it(`Before email verification: Profile endpoint should work`, async function(){
        const res = await cli1.get_profile()
        assert.equal(res.body.email, data1.email, "Check that profile response contains user data")
    })

    it(`Before email verification: Other endpoints should not work`, async function(){
        await cli1.get_events().then( res => assert.equal(res.statusCode, 403) )
        await cli1.my_requests().then( res => assert.equal(res.statusCode, 403) )
    })

    it(`user 1 verifies email`, async function(){
        const code = await cli1.get_email_verification_code()
        const res = await cli1.verify_email({ body: {code} })
    })

    // ----------------------------------------------
    //    On Register: (before email verification)
    // ----------------------------------------------

    it(`After email verification: Other endpoints should work`, async function(){
        await cli1.get_events().then( res => assert.equal(res.statusCode, 200) )
        await cli1.my_requests().then( res => assert.equal(res.statusCode, 200) )
    })

    // ----------------------------------------------
    //    Email Change
    // ----------------------------------------------
    it(`user 1 changes email`, async function(){
        const res = await cli1.update_email({
            body: {
                email: new_data1.email
            }
        })
        assert.equal(res.statusCode, 200)
    })

    it(`login with current credentials works`, async function(){
        const res = await cli1.login({ body: data1 })
        assert.equal(res.statusCode, 200, "login should 200")
        assert.isNotNull(res.body.token, 'Should return savepoint token')
    })

    it(`login attempt with new credentials should return appropriate error`, async function(){
        const res = await cli1.login({ body: { email: new_data1.email, password: data1.password } })
        assert.equal(res.statusCode, 401, "login should 401")
        assert.equal(res.body.token, null, 'should not return sp token')
    })

    it(`user 1 verifies new email`, async function(){
        const code = await cli1.get_email_verification_code()
        const res = await cli1.verify_email({ body: {code} })
    })

    it(`new credentials now work`, async function(){
        const res = await cli1.login({ body: { email: new_data1.email, password: data1.password } })
        assert.equal(res.statusCode, 200, "login should 200")
        assert.isNotNull(res.body.token, 'Should return savepoint token')
    })

    it(`old credentials no longer work`, async function(){
        const res = await cli1.login({ body: data1 })
        assert.equal(res.statusCode, 401, "login should 401")
        assert.equal(res.body.token, null, 'should not return sp token')
    })


})