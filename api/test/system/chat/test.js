const bluebird   = require('bluebird')
const postgres   = require('../../../src/common/postgres/tables.js')
const ChatClient = require('../../clients/ChatClient.js')
const SPClient   = require('../../clients/SPClient.js')
const assert     = require('assert')

describe(`chat`, function(){

    let cli, 
        api_cli_1,
        api_cli_2,
        chat_cli_1,
        chat_cli_2,
        group_id

    before(async function(){

        // Get a valid group to subscribe to
        api_cli1 = new SPClient('localhost:8000')
        const res = await api_cli1.login({
            body: {
                email: `user0@savepoint.party`,
                password: `password0`
            }
        })
        api_cli1.authenticate(res.body.token)
        const matched = (await api_cli1.my_requests()).body.matched
        group_id = matched[0].group.id

        chat_cli1  = new ChatClient('chat:8080')        
        chat_cli2  = new ChatClient('chat:8080')        
        chat_cli1.send({
            action: "connect",
            group_id,
            user_id: 1
        })

        chat_cli2.send({
            action: "connect",
            group_id,
            user_id: 2
        })
    })

    
    it(`start chatting`, async function(){

        chat_cli1.send({
            action: "message",
            message: "Hello!"
        })

        const m1 = await chat_cli2.next()
        const message = JSON.parse(m1)
        assert.equal(message.message, "Hello!")

    })

    it('chat history', async function(){
        const res = await api_cli1.chat_history(group_id)
        // assert.equal(res.length, 2)
    })
    

})
