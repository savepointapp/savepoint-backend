'use strict'
const _    = require('lodash')
const chai = require('chai')
chai.use(require('chai-http'))

module.exports = class BaseClient {

    constructor(host) {

        this.headers = {} 

        // host = where we send requests
        this.host  = host

        // agent = the thing that makes request to the host.
        this.agent = chai.request.agent(host)

    }

    // Some sugar for defining endpoints
    // Don't try to read this.
    endpoint (method, _path){
        return async function(data={}, params={}){

            const path = _.chain(_path)
            .split('/')
            .map(token =>{
                if (token[0] == ':' ) {
                    let value = params[token.substring(1)]
                    if(value == null){
                        throw new Error(`${method} call to ${_path} is missing param ${token.substring(1)}.`)
                    }
                    return value
                } else {
                    return token
                }
            })
            .join('/')
            .value()

            const {
                headers,
                form_data,
                body,
                attachments,
                query_strings,
                json,
            } = data

            const request = this.agent[method.toLowerCase()](path)
            .set(headers || this.headers)
            .query(query_strings)
            .type('json')

            if (form_data){
                _.forOwn(form_data, (value, key) => {
                    request.field(key, value)
                })
            }

            if (body){
                request.send(body)
            }

            _.forOwn(attachments, (value, key) => {
                request.attach(key, value)
            })

            try { 
                return await request
            } catch (err) {
                return err
            }

        }
    }
}