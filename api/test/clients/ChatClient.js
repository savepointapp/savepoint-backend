const WebSocket = require('ws');

module.exports = class ChatClient {
    constructor(base_url){
        this.ws = new WebSocket(`ws://${base_url}/ws`);
        this.base_url
        this.messages = []
        this.idx = 0
        this.ws.on('message', data => this.receive(data))
        this.initPromise = this.init()
    }
    init(){
        return new Promise(resolve => this.ws.on('open', resolve))
    }
    send(data){
        return this.initPromise.then(()=>this.ws.send(JSON.stringify(data)))
    }    
    next(){
        if (this.messages.length > 0) {
            return Promise.resolve(message)
        }
        else {
            return new Promise( resolve => {
                this.message_cb = resolve
            })
        }
    }
    receive(data){
        if (this.message_cb == null){
            this.messages.push(data)
        }
        else {
            this.message_cb(data)
        }
    }
}