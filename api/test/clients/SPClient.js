const TestClient = require('./TestClient.js')
const pg         = require('../../src/common/clients/pg.js').getClient()

module.exports = class SPClient extends TestClient {

    constructor (base_url) {

        super(base_url)
        this.base_url = base_url

        // Endpoints 
        this.register               = this.endpoint( "POST" , "/api/v1/register/" )
        this.login                  = this.endpoint( "POST" , "/api/v1/login/"    )
        this.get_profile            = this.endpoint( "GET"  , "/api/v1/profile/"  ) 
        this.update_profile         = this.endpoint( "PUT"  , "/api/v1/profile/"  ) 
        this.update_email           = this.endpoint( "PUT"  , "/api/v1/email/"    ) 
        this.verify_email           = this.endpoint( "POST" , "/api/v1/email/verify") 
        this.forgot_password        = this.endpoint( "POST" , "/api/v1/forgot_password/" ) 
        this.request_password_change = this.endpoint( "POST" , "/api/v1/request_password_change/"  ) 
        this.confirm_password_change = this.endpoint( "POST" , "/api/v1/confirm_password_change/"  ) 
        this.get_events             = this.endpoint( "GET"  , "/api/v1/events/"  ) 
        this.my_requests            = this.endpoint( "GET"  , "/api/v1/requests/"  ) 
        this.new_request            = this.endpoint( "POST" , "/api/v1/requests/"  ) 
        this.cancel_request         = this.endpoint( "DELETE" , "/api/v1/requests/:request_id"  ) 
        this.update_room            = this.endpoint( "PUT"  , "/api/v1/rooms/:room_id"  )   
        this.report                 = this.endpoint( "POST" , "/api/v1/reports"  )   
        this._get_group             = this.endpoint( "GET"  , "/api/v1/groups/:group_id"  ) 
        this._chat_history          = this.endpoint( "GET" , "/api/v1/groups/:group_id/chat"  ) 
        this._kick_member            = this.endpoint( "POST", '/api/v1/groups/:group_id/remove' )

        // Admin Endpoints
        this.make_group             = this.endpoint( "POST" , "/api/v1/match/"  ) 
        this.split_group            = this.endpoint( "POST" , "/api/v1/split/"  ) 
        this.view_reports           = this.endpoint( "GET"  , "/api/v1/reports"  ) 
    }

    authenticate(token){
        this.headers['x-access-token'] = token
    }

    async reg_and_verify (params) {
        const res = await this.register(params)
        this.authenticate(res.body.token)
        const code = await this.get_email_verification_code()
        await this.verify_email({ body: {code} })
    }

    async set_admin () {
        // const profile_id = (await this.get_profile()).body.id
        const profile_id = 5
        await pg.query(`
            update users
               set is_superuser = true
             where id = $<profile_id>`
        , { profile_id})
    }

    kick_member(group_id, user_id){
        return this._kick_member({body: {user_id}}, { group_id })
    }

    get_group(group_id){
        return this._get_group({},{ group_id })
    }

    chat_history(group_id){
        return this._chat_history({},{ group_id })
    }

    get_my_id(){
        return this.get_profile().then( res => res.body.id )
    }

    async reg_and_auth (body){
        const res = await this.register({body})
        this.authenticate(res.body.token)
        this.id = await this.get_my_id()
    }

    async get_email_verification_code(){
        const user_id = await this.get_my_id()
        const res = await pg.one(`
            select *
              from verification_codes
             where user_id = $<user_id>
               and is_used = false`
        , { user_id })
        return res.code
    }

    async get_code({  email, op }){
        const res = await pg.one(`
            select *
              from codes as c 
              join users as u
                on c.user_id = u.id
             where u.email = $<email>
               and op = $<op>`
        , { email, op })
        return res.code
    }


}