const path            = require('path')
const router          = require('koa-router')()
const authenticate    = require('../middleware/authenticate.js')
const email_required  = require('../middleware/email_required.js')
const admin_only      = require('../middleware/admin_only.js')

/* Ping checks */
router.post( '/api/v1/ping'             ,                                 require('./requests/ping/pong.js') )

/* Authentication / Profile */
router.post( '/api/v1/register'         ,                                 require('./requests/user/register.js') )
router.post( '/api/v1/login'            ,                                 require('./requests/user/login.js') )
router.get(  '/api/v1/profile'          , authenticate ,                  require('./requests/user/get_profile.js') )
router.put(  '/api/v1/profile'          , authenticate ,                  require('./requests/user/update_profile.js') )
router.put(  '/api/v1/email'            , authenticate ,                  require('./requests/user/update_email.js') )
router.post( '/api/v1/email/verify'     , authenticate ,                  require('./requests/user/verify_email.js') )

router.post( '/api/v1/forgot_password'         ,                          require('./requests/user/forgot_password.js') )
router.post( '/api/v1/request_password_change' , authenticate ,            require('./requests/user/request_password_change.js') )
router.post( '/api/v1/confirm_password_change' ,                          require('./requests/user/confirm_password_change.js') )
  
/* Events */
router.get(  '/api/v1/events'           , authenticate , email_required , require('./requests/events/list.js') )
router.get(  '/api/v1/events/:event_id'           , authenticate , admin_only , require('./requests/events/get_event.js') )
router.post( '/api/v1/events'           , authenticate , admin_only     , require('./requests/events/create.js') )
router.put(  '/api/v1/events/:event_id' , authenticate , admin_only ,     require('./requests/events/update.js'))

/* Room Requests + Groups */
router.post( '/api/v1/requests'         , authenticate , email_required , require('./requests/requests/create.js') )
router.get(  '/api/v1/requests'         , authenticate , email_required , require('./requests/requests/list.js') )
router.get(  '/api/v1/requests/all'         , authenticate , admin_only , require('./requests/requests/list_all.js') )
router.delete(  '/api/v1/requests/:request_id', authenticate , email_required , require('./requests/requests/delete.js') )
router.get(  '/api/v1/groups/:group_id' , authenticate , email_required , require('./requests/groups/get.js') )
router.post( '/api/v1/groups/:group_id/remove' , authenticate , email_required , require('./requests/groups/remove.js') )
router.post( '/api/v1/groups/:group_id/unmatch' , authenticate , admin_only , require('./requests/groups/remove_group.js') )
router.put(  '/api/v1/rooms/:room_id'   , authenticate , email_required , require('./requests/rooms/update.js') )

/* Matching */
router.post(  '/api/v1/match'           , authenticate , admin_only     , require('./requests/groups/create.js') )

/* Reporting shitty users */
router.post(  '/api/v1/reports'         , authenticate , email_required , require('./requests/reports/create.js') )
router.get(   '/api/v1/reports'         , authenticate , admin_only     , require('./requests/reports/list.js') )

/* Chat history */ 
router.get(  '/api/v1/groups/:group_id/chat' , authenticate , email_required , require('./requests/messages/list.js') )

module.exports = router