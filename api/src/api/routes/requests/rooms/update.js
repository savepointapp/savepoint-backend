const _ = require('lodash')
const config = require("../../../../common/config.js")
const db     = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')

const base_fields = [
    'accommodation_name',
    'address_line_1',
    'address_line_2',
    'zipcode',
    'city',
    'state',
    'other_details',
]

const special_fields = [
    'check_in_date',
    'check_out_date',
    'price_total',
]
    
module.exports = async function(ctx, next){
    const user_id = ctx.request.user.id
    const room_id = ctx.params.room_id

    const req  = await db.savepoint_requests.fetch_raw_by_room_id(room_id)
    const room = await db.rooms.get(room_id)

    // Permission Denied
    if (user_id !== req.user_id){
        throw new SavepointError({ code: 403 })
    }

    // disallow certain fields for already matched requests.
    const allowed_fields = req.group_id ? base_fields 
                                        : _.concat(base_fields, special_fields)

    ctx.body = await db.rooms.update({
        id: room_id,
        ..._.pick(ctx.request.body, allowed_fields)        
    })
}

