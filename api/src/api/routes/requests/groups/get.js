const config = require("../../../../common/config.js")
const db     = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')


/*

3 0 min goal :-O

get group endpoint 

add to requests test
add to routes 
make endpoint 
make postgres shit work 
run test

*/

module.exports = async function(ctx, next){
    const user_id = ctx.request.user.id
    const group_id = ctx.params.group_id
    
    if ( !(await db.groups.contains_member({ group_id , user_id }) ) ){
        // console.log('wtf')
        throw new SavepointError({ code: 403 })
    }

    const group = await db.groups.fetch_by_id_v2(group_id)
    // console.log('G', JSON.stringify(group,undefined,2))
    ctx.body =  group
}

