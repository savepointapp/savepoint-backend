const bluebird          = require('bluebird')
const _                 = require('lodash')
const config            = require("../../../../common/config.js")
const pg                = require('../../../../common/clients/pg.js').getClient()
const db                = require('../../../../common/postgres/tables.js')
const SavepointError    = require('../../../../common/errors/SavepointError.js')
const mailer            = require('../../../../common/mail/mailer.js')

module.exports = async function(ctx, next){
    console.log('create group');

    const request_ids = ctx.request.body.request_ids

    if (request_ids.length < 2){
        throw new SavepointError({code:400, message: "must have at least 2 in a group"})
    }

    if (request_ids.length > 20){
        throw new SavepointError({code:400, message: "must have less than 20 in a group"})
    }

    // TODO Don't be stupid, make this a single query.
    const requests = await bluebird.map(request_ids, id => db.savepoint_requests.fetch(id))
    // Limit the number of rooms present.
    const room_ids = _.map(requests, 'room.id')
    console.log(`Found the rooms: ${room_ids}`)
    const rooms = _.filter(room_ids, id => id != null)
    if (rooms.length > 1){
        throw new SavepointError({ code: 400, message: "Too many members with rooms! Limit to 1 please :-)"})
    }
    
    // Validated above so this is safe
    const room_id = rooms[0]
    await pg.tx(async t => {
        const group = await db.groups.create({room_id}, t)
        console.log(`created group`, group)
        const update_queries = _.map(request_ids, request_id => db.savepoint_requests.set_group({
            request_id, 
            group_id: group.id
        }, t))
        await t.batch( update_queries )
    })

    //get list of users based on user_id
    const user_ids = _.map(requests, 'user_id')
    const users = await bluebird.map(user_ids, id => db.user.lookup_by_id(id))
    
    //get list of emails based on users
    const emails = _.map(users, 'email')
    console.log('found the emails')

    emails.forEach(send_emails)
    
    ctx.body = { message: "Success!" } // This is absolute shit.
                                       // TODO fix this so its not shit.
}

async function send_emails( email ){
    if(email == null){
        throw new SavepointError({ code: 400, message: "You Dope, Some users have no email"})
    }
    //send everyone emails
    console.log(`email: ${email}`)
    await mailer.send_matched_request(email)
}