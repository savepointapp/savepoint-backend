const config = require("../../../../common/config.js")
const _      = require('lodash')
const db     = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')
module.exports = async function(ctx, next){
    const user_id = ctx.request.user.id
    const group_id = ctx.params.group_id
    const kicked_member_id = ctx.request.body.user_id

    console.log(JSON.stringify(ctx))
    
    // valiate user owns group.
    if ( !await db.groups.is_owner({ group_id, user_id}) ) {
        throw new SavepointError({ code: 403 })
    }

    if ( !(await db.groups.contains_member({ group_id , user_id: kicked_member_id }) ) ){
        throw new SavepointError({ code: 404, message: "group member doesn't exist" })
    }

    const requests = await bluebird.map(request_ids, id => db.savepoint_requests.fetch(id))
    const group_object = await bluebird.map(group_id, id => db.groups.fetch_by_id_v2(id))

    const user_ids = _.map(group_object, 'user.id')
    console.log(`Found the rooms: ${user_ids}`)
    const users = await bluebird.map(user_ids, id => db.user.lookup_by_id(id))


    //get list of emails based on users
    const emails = _.map(users, 'email')
    console.log('found the emails')

    emails.forEach(send_emails)

    const group = await db.savepoint_requests.disband_group({ 
        group_id
    })

    // console.log('G', JSON.stringify(group,undefined,2))
    ctx.body =  { message: 'deleted' }
}

async function send_emails( email ){
    if(email == null){
        throw new SavepointError({ code: 400, message: "You Dope, Some users have no email"})
    }
    //send everyone emails
    console.log(`email: ${email}`)
    await mailer.send_unmatched_request(email)
}