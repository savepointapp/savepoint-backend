const config = require("../../../../common/config.js")
const _      = require('lodash')
const bluebird = require('bluebird')
const db     = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')
const mailer            = require('../../../../common/mail/mailer.js')

module.exports = async function(ctx, next){
    const user_id = ctx.request.user.id
    const group_id = ctx.params.group_id
    const kicked_member_id = ctx.request.body.user_id
    console.log(JSON.stringify(ctx.params))
    
    // valiate user owns group.
    if ( !await db.groups.is_owner({ group_id, user_id}) ) {
        throw new SavepointError({ code: 403 })
    }

    if ( !(await db.groups.contains_member({ group_id , user_id: kicked_member_id }) ) ){
        throw new SavepointError({ code: 404, message: "group member doesn't exist" })
    }

    const group1 = await db.groups.fetch_by_id_v2(group_id)

    const emails = group1.members.map(m => m.member.email)

    await bluebird.map(emails, email => send_emails(email))

    const group = await db.savepoint_requests.disband_group({ 
        group_id
    })

    ctx.body =  { message: `deleted`}
}

async function send_emails( email ){
    if(email == null){
        throw new SavepointError({ code: 400, message: "You Dope, Some users have no email"})
    }
    //send everyone emails
    console.log(`sending email to: ${email}`)
    await mailer.send_unmatched_request(email)
}