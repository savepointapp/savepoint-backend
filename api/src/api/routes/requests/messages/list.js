const db             = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')
const config         = require('../../../../common/config.js')

module.exports = async function(ctx, next){

    const group_id   = ctx.params.group_id
    const user_id    = ctx.request.user.id 

    const has_permission = await db.groups.contains_member({user_id, group_id})

    // User must be in the group
    if (!has_permission){
        throw new SavepointError({code:403})
    }


    const messages = await db.messages.filter({
        group_id,
        limit    : 10000, //ctx.query.limit  || config.MESSAGE_PAGE_SIZE,
        offset   : ctx.query.offset || 0
    })

    ctx.body = messages

}
