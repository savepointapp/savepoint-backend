const db = require('../../../../common/postgres/tables.js')

module.exports = async function(ctx, next){

    const start_date = ctx.query.start
    const end_date   = ctx.query.end
    const limit      = ctx.query.limit
    const offset     = ctx.query.offset

    // const events = await db.events.search({
    //     start_date,
    //     end_date,
    //     limit,
    //     offset
    // })

    // For now we just return all ofthe events.
    // See above for what filtering will look like when we get to it finally.s
    const events = await db.events.all()

    ctx.body = events

}
