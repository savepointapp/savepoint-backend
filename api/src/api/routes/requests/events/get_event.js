const config = require("../../../../common/config.js")
const db = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')

module.exports = async function(ctx, next){
    const event_id    = ctx.params.event_id

    const event = await db.events.fetch_by_id(event_id)
    console.log(JSON.stringify(event))
    ctx.body = event
}
