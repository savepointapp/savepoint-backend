const db     = require('../../../../common/postgres/tables.js')

module.exports = async function(ctx, next){

    const id            = ctx.params.event_id
    const name          = ctx.request.body.name
    const description   = ctx.request.body.description
    const image_url     = ctx.request.body.image_url
    const start_date    = ctx.request.body.start_date
    const end_date      = ctx.request.body.end_date
    const city          = ctx.request.body.city
    const state         = ctx.request.body.state

    const event = await db.events.update({
        id,
        name,
        description,
        image_url,
        start_date,
        end_date,
        city,
        state
    })

    ctx.body = event
}
