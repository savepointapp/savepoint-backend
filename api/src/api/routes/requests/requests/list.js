const config = require("../../../../common/config.js")
const db     = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')

module.exports = async function(ctx, next){
    const user_id = ctx.request.user.id

    const all_requests = await db.savepoint_requests.list_for_user_id(user_id)

    ctx.body = {
        matched   : all_requests.filter( r => r.group != null ) ,
        unmatched : all_requests.filter( r => r.group == null )
    }
}

