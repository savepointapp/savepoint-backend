const _      = require('lodash')
const config = require("../../../../common/config.js")
const pg     = require('../../../../common/clients/pg.js').getClient()
const db     = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')

module.exports = async function(ctx, next){

    const user_id     = ctx.request.user.id
    const request_id  = ctx.params.request_id

    // Fetch and format request_data
    ctx.body = await db.savepoint_requests.delete(request_id)
}








