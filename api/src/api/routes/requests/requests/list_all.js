const config = require("../../../../common/config.js")
const db     = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')

module.exports = async function(ctx, next){
    const all_requests = await db.savepoint_requests.all_requests()

    ctx.body = {
        matched   : all_requests.filter( r => r.group != null ) ,
        unmatched : all_requests.filter( r => r.group == null )
    }
}

