const _      = require('lodash')
const config = require("../../../../common/config.js")
const pg     = require('../../../../common/clients/pg.js').getClient()
const db     = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')
const create_request = require('../../../includes/requests/create.js')

module.exports = async function(ctx, next){

    const user_id     = ctx.request.user.id
    const event_id    = ctx.request.body.event_id

    const room        = ctx.request.body.room
    const prefs       = ctx.request.body.prefs
    const room_prefs  = ctx.request.body.room_prefs

    const request_id = await create_request({
        event_id,
        user_id,
        room,
        prefs,
        room_prefs
    })

    // Fetch and format request_data
    const res = await db.savepoint_requests.fetch(request_id)
    ctx.body = res
}








