'use strict'
const bcrypt                     = require('bcrypt')
const config                     = require('../../../../common/config.js')
const SavepointError             = require('../../../../common/errors/SavepointError.js')
const postgres                   = require('../../../../common/postgres/tables.js')
const mailer                     = require('../../../../common/mail/mailer.js')
const generate_verification_code = require('../../../includes/user/generate_verification_code.js')

module.exports = async function(ctx, next) {

    // Get user with this email as verified
    const user  = ctx.request.user
    const email = user.email

    // Store initializes transaction
    // TODO put some restrictions on this.
    await postgres.codes.insert({
        user_id : user.id,
        code    : generate_verification_code(config.PASSWORD_VERIFICATION_CODE_LENGTH),
        op      : 'reset_password'
    })

    console.log(`Sending reset_password verification code to ${email}`) ;
    await mailer.send_reset_password_code({ email, code }) 

    ctx.body = { message: "Check your email for a verification code."}
}