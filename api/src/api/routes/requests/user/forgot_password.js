'use strict'
const bcrypt                     = require('bcrypt')
const config                     = require('../../../../common/config.js')
const SavepointError             = require('../../../../common/errors/SavepointError.js')
const postgres                   = require('../../../../common/postgres/tables.js')
const mailer                     = require('../../../../common/mail/mailer.js')
const generate_verification_code = require('../../../includes/user/generate_verification_code.js')

module.exports = async function(ctx, next) {

    const email = ctx.request.body.email

    // Get user with this email as verified
    const user = await postgres.user.lookup_by_email(email)
    const code = generate_verification_code(config.PASSWORD_VERIFICATION_CODE_LENGTH)

    // Store initializes transaction
    // TODO put some restrictions on this.
    await postgres.codes.insert({
        user_id: user.id,
        code,
        op: 'reset_password'
    })

    console.log(`Sending reset_password verification code to ${email}`) ;
    await mailer.send_reset_password_code({ email, code }) 

    ctx.body = { message: "Check your email for a verification code."}
}