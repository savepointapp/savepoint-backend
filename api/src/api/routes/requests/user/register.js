'use strict'
const bcrypt         = require('bcrypt')
const jwt            = require('jsonwebtoken');
const config         = require("../../../../common/config.js")
const fb             = require('../../../../common/clients/fb.js')
const pg             = require('../../../../common/clients/pg.js').getClient()
const SavepointError = require('../../../../common/errors/SavepointError.js')
const mailer         = require('../../../../common/mail/mailer.js')
const generate_verification_code = require('../../../includes/user/generate_verification_code.js')
const db             = require('../../../../common/postgres/tables.js')


module.exports = async function(ctx, next) {

    // Client sends us a facebook access code for the user.
    const {
        token,
        username,
        password,
        email,
        bio,
        first_name,
        last_name,
        email_sub,
        birthday,
        tos,
        gender,
        sex,
        image_url
        } = ctx.request.body

    let new_account

    // Facebook signup
    
    if (token) {

        console.log(`received token`)
        
        const fbuid = await fb.verify(token) // Throws 400

        new_account = await pg.tx( async t => {
            const user = await db.user.create({
                username  ,
                email     ,
                bio       ,
                first_name,
                last_name ,
                email_sub ,
                birthday       ,
                tos       ,
                gender    ,
                sex,
                image_url ,
            })
            await db.user.create_social_account({
                social_id: fbuid,
                provider: 'facebook',
                user_id:  user.id
            })
            return user
        }).catch(err => {
            console.log("ERR IN FB REG", err)
            if (err.code == 23505){ // Unique violation
                throw new SavepointError({ code: 400, message: "Account already exists."})
            }
        })

    // Regular signup
    } else if (email && password) {

        console.log(`received email / password`)

        const encrypted_password = await bcrypt.hash(password, config.BCRYPT_SALT_ROUNDS)

        new_account = await db.user.create({
            username           ,
            encrypted_password ,
            email              ,
            bio        ,
            first_name ,   
            last_name  ,  
            email_sub ,
            birthday       ,
            tos       ,
            gender    ,
            sex     ,
            image_url ,
        }).catch(err => {
            console.log(`error while creating email pass acct`, err)
            if (err.code == 23505){ // Unique violation
                throw new SavepointError({ code: 400, message: "Account already exists."})
            }
        })

        const code  = await generate_verification_code(config.EMAIL_VERIFICATION_CODE_LENGTH)
        await db.verification_codes.create({ 
            user_id: new_account.id ,
            email,
            code
        })

        // console.log(`Sending verification code to ${email}`)
        // await mailer.send_email_verification_code({ email, code }) 


        // console.log("WOW", new_account)

    } else {
        throw new SavepointError({ code: 400, message: "token or email/username required"})
    }

    // JSONWebTokens 
    const sptoken = jwt.sign({ id: new_account.id }, config.AUTH_SECRET, {
        expiresIn: 86400 
    });

    ctx.body = { token: sptoken }
    ctx.status = 200

}
