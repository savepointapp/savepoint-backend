const bcrypt = require('bcrypt')
const jwt    = require('jsonwebtoken');
const config = require("../../../../common/config.js")
const fb     = require('../../../../common/clients/fb.js')
const db     = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')

module.exports = async function(ctx, next) {

    // Client sends us a facebook access code for the user.
    const {
        token,
        password,
        email,
        } = ctx.request.body

    let user

    if (token) {    

        const fbuid = await fb.verify(token) // Throws 400
        user = await db.user.lookup_from_social_id(fbuid, 'facebook').catch( err => {
            throw new SavepointError({code:401})
        })
        console.log("USER", user)

    } else if (email && password) {

        user = await db.user.lookup_by_email(email).catch( err => {
            throw new SavepointError({code:401})
        })
        if(!(await bcrypt.compare(password, user.encrypted_password))){
            throw new SavepointError({ code: 400, message: "Couldn't log user in"})
        }

    } else {
        throw new SavepointError({ code: 400, message: "token or password/username required"})
    }

    // generate token for user 
    const sptoken = jwt.sign({ id: user.id }, config.AUTH_SECRET, {
        expiresIn: 86400 
    });

    ctx.body = { token: sptoken }
    ctx.status = 200

}
