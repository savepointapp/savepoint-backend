'use strict'
const config                     = require('../../../../common/config.js')
const postgres                   = require('../../../../common/postgres/tables.js')
const verify_email               = require('../../../includes/user/verify_email.js')

module.exports = async function(ctx, next) {

    const user_id = ctx.request.user.id
    const code    = ctx.request.body.code
    
    // Throws an error if something goes wrong.
    await verify_email(user_id, code)

    ctx.body = { 
        message : "Success",
        also    : "fuck you"
    }
}