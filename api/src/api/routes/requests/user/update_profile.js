const db     = require('../../../../common/postgres/tables.js')
const _      = require('lodash')
/* Update any fields that don't require further validation*/
module.exports = async function(ctx, next) {
    const user_id = ctx.request.user.id

    const {
        username  ,
        email     ,
        bio       ,
        first_name,
        last_name ,
        email_sub ,
        age       ,
        tos       ,
        gender    ,
        image_url
    } = ctx.request.body

    const res = await db.user.update({
        id: user_id ,
        username    ,
        email       ,
        bio         ,
        first_name  ,
        last_name   ,
        email_sub   ,
        age         ,
        tos         ,
        gender      ,
        image_url
    })

    ctx.body = _.omit(res, 'encrypted_password')

}