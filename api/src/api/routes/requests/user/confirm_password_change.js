'use strict'
const bcrypt                     = require('bcrypt')
const config                     = require('../../../../common/config.js')
const SavepointError             = require('../../../../common/errors/SavepointError.js')
const postgres                   = require('../../../../common/postgres/tables.js')
const mailer                     = require('../../../../common/mail/mailer.js')
const generate_verification_code = require('../../../includes/user/generate_verification_code.js')

module.exports = async function(ctx, next) {
    const code     = ctx.request.body.code
    const password = ctx.request.body.password
    const encrypted_password = await bcrypt.hash(password, config.BCRYPT_SALT_ROUNDS)
    const the_code = await postgres.codes.from_code(code)

    // bad op
    if (the_code.op !== 'reset_password'){
        throw new SavepointError({ code: 403 })
    }

    await postgres.user.update({
        id: the_code.user_id,
        encrypted_password 
    })

    ctx.body = { message: "fuck you" }
}