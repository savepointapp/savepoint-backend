const db     = require('../../../../common/postgres/tables.js')
const _      = require('lodash')

/* 
    firstName,
    lastName,
    email,
    bio,
    gendre,
    age,
    subscribe_to_update_emails,
    tos, 
*/
module.exports = async function(ctx, next) {
    const user_id = ctx.request.user.id
    const res = await db.user.lookup_by_id(user_id)
    ctx.body = _.omit(res, 'encrypted_password')
}