'use strict'
const config                     = require('../../../../common/config.js')
const db                         = require('../../../../common/postgres/tables.js')
const mailer                     = require('../../../../common/mail/mailer.js')
const verify_email               = require('../../../includes/user/verify_email.js')
const generate_verification_code = require('../../../includes/user/generate_verification_code.js')

module.exports = async function(ctx, next) {

    const email = ctx.request.body.email
    const user = ctx.request.user

    if ( !user.is_email_verified ){
        // Users who change their email before having a verified email will
        // also need their profile email updated.
        await db.user.update({ id: user.id , email })
    }

    const code  = await generate_verification_code(config.EMAIL_VERIFICATION_CODE_LENGTH)
    await db.verification_codes.create({ user_id: user.id, email, code })

    console.log(`Sending verification code to ${email}`)
    await mailer.send_email_verification_code({ email, code }) 

    ctx.body = { message: "fuck you, or something."}
}