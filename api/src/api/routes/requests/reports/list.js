const bluebird = require('bluebird')
const _        = require('lodash')
const config = require("../../../../common/config.js")
const db     = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')

module.exports = async function(ctx, next){
    
    // Filter by user_id, group_id, created_on 
    const {user_id, group_id} = ctx.query

    // TODO Add filters
    ctx.body = await db.reports.select({user_id, group_id})

}

