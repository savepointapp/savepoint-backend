const bluebird = require('bluebird')
const _        = require('lodash')
const config = require("../../../../common/config.js")
const db     = require('../../../../common/postgres/tables.js')
const SavepointError = require('../../../../common/errors/SavepointError.js')

module.exports = async function(ctx, next){

    const user_id = ctx.request.user.id

    const {
        other_user_id,
        group_id,
        reason
    } = ctx.request.body

    const report = await db.reports.create({
        user_id,
        other_user_id,
        group_id,
        reason
    })
    
    ctx.body = report
}

