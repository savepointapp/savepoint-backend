module.exports = async function(ctx, next) {
    ctx.request.body = await ctx.request.getBody()
    await next()
}