const SavepointError = require('../../common/errors/SavepointError.js')

module.exports = async function(ctx, next) {
    try {
        await next()
    } catch (err) {
        console.log(err)
        // SavepointError class is used to report
        // errors to the user
        if (err instanceof SavepointError){
            ctx.status = err.code || 500
            ctx.body = {
                code: err.code || 500,
                message: err.message
            }
        // If its not a SavepointError then we fucked up.
        } else {
            ctx.status = 500
            ctx.body = {
                code: 500,
                message: "Internal Server Error"
            }
        }
    }
}