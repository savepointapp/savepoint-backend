const config = require('../../common/config.js');
const SavepointError = require('../../common/errors/SavepointError.js')

module.exports = async function(ctx, next) {
    const user = ctx.request.user
    if (!user.email || !user.is_email_verified){
        throw new SavepointError({ code: 403, message: "user needs a verified email"})
    }

    await next()
}