const jwt    = require('jsonwebtoken');
const config = require('../../common/config.js');
const SavepointError = require('../../common/errors/SavepointError.js')
const postgres = require('../../common/postgres/tables.js')

module.exports = async function(ctx, next){
    const user_id = ctx.request.user.id
	console.log(user_id);
    
    const user = await postgres.user.lookup_by_id(user_id)
    if (!user.is_superuser){
        throw new SavepointError({code:403})
    }
    await next()
}