const jwt    = require('jsonwebtoken');
const config = require('../../common/config.js');
const SavepointError = require('../../common/errors/SavepointError.js')
const db     = require('../../common/postgres/tables.js')

module.exports = async function(ctx, next){

    const token = ctx.headers['x-access-token'];
    if (!token){
        throw new SavepointError({ code: 401, message: "User not authenticated."})
    }

    // "Promisify" jwt
    const data = await new Promise(function(resolve, reject){
        jwt.verify(token, config.AUTH_SECRET, function(err, data){
            if (err) return reject(err)
            resolve(data)
        })
    }).catch(err => {
        throw new SavepointError({ code: 401, message: "Could not authenticate token" })
    })

    ctx.request.user = await db.user.lookup_by_id(data.id).catch( err => {
        throw new SavepointError({code:404})
    })

    await next()

}