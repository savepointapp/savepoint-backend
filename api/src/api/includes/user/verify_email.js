const config   = require('../../../common/config.js')
const db = require('../../../common/postgres/tables.js')
const SavepointError = require('../../../common/errors/SavepointError.js')

module.exports = async function(user_id, code){
    console.log("USER ID CODE", user_id, code)
    const verification_code = await db.verification_codes.lookup({ user_id, code })
    .catch(err => { throw new SavepointError({ code: 404, message: "No such code" }) })

    // console.log('code', code, 'v', verification_code)
    // Lock them out on too many attempts
    if (verification_code.attempts > config.EMAIL_VERIFICATION_MAX_ATTEMPTS){
        console.log(`Verification attempts too big: ${verification_code.attempts}`)
        throw new SavepointError({ code: 403, message: "Too many verification attempts"}) 
    }

    // Lock them out on expired code
    const created_on = new Date(verification_code.created_on).valueOf()
    if (Date.now() - created_on > config.EMAIL_VERIFICATION_CODE_TTL){
        console.log(`Verification code is expired: ${verification_code.attempts} attempts`)
        throw new SavepointError({ code: 403, message: "Expired code" }) 
    }

    // Success: Copy pending email into user profile + set is_email_verified
    await db.user.update({
        id                : user_id,
        email             : verification_code.email,
        is_email_verified : true
    }) 

    await db.verification_codes.set_used({ user_id, code})

}