const bluebird = require('bluebird')
const _        = require('lodash')
const pg = require('../../../common/clients/pg.js').getClient()
const db = require('../../../common/postgres/tables.js')

module.exports = function({
    event_id,
    user_id,
    room,
    prefs,
    room_prefs
}){

    // console.log(`MAKING REQUEST WITH PARAMS: `, {
    //     event_id,
    //     user_id,
    //     room,
    //     prefs,
    //     room_prefs
    // })
    
    return pg.tx( async t => {
        const object_ids = await bluebird.props({
            room_id: room 
                ? db.rooms.create(room, t).then(obj => obj.id)
                : bluebird.resolve(null) ,
            room_preferences_id: room_prefs
                ? db.room_preferences.create(room_prefs, t).then(obj => obj.id)
                : bluebird.resolve(null) ,
            general_preferences_id: db.general_preferences.create(prefs, t).then(obj => obj.id)
        })
        const data = _.merge(object_ids, { user_id, event_id }) 
        const request_id = await db.savepoint_requests.create( data, t ).then(obj => obj.id)
        return request_id
    })
}
