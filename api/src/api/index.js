'use strict'

const koa                   = require('koa') ;

const kcors                 = require('kcors');
const convert               = require('koa-convert');

const get_body              = require('koa-get-body') ;
const argv                  = require('minimist')(process.argv.slice(2))
const error_catcher         = require('./middleware/error_catcher.js')
const form_and_body_parser  = require('./middleware/form_and_body_parser.js')
const router                = require('./routes/routes.js')
const config                = require('../common/config.js')


const cors_settings = {
    origin: '',
    credentials: true,
    headers: 'Origin, X-Requested-With, Content-Type, Accept'
}

// This is a koa app
const app                   = new koa()

// These middlewares will be run on every request in this order:
app.use(error_catcher)
app.use(async function(ctx,next){
    // console.log(`${ctx.method} ${ctx.path} IN`)
    await next() 
    console.log(`${ctx.method} ${ctx.path} ${ctx.status}`)
})
app.use(get_body())
app.use(form_and_body_parser)

app.use(convert(kcors(cors_settings)));

// Finally, we execute route specific middleware specified in routes.js
app.use(router.routes())

module.exports = app.listen(config.SAVEPOINT_API_PORT, () => {
    console.log("Savepoint webserver running on port ", config.SAVEPOINT_API_PORT)
})