const argv     = require('minimist')(process.argv.slice(2))
const env      = process.env
// const heapdump = require('heapdump')  // never know when you'll need this...

const config = {}

// Web
config.SAVEPOINT_API_PORT = 8000

// Crypto + Auth
config.BCRYPT_SALT_ROUNDS = 12
config.AUTH_SECRET = '60i63Ln58mue4iob9XncK4yl8wBnbYyya4lYJVaz0QgdPgf1mhFhjoXGRUY='

// Email Verification
config.EMAIL_VERIFICATION_MAX_ATTEMPTS = 5 ;
config.EMAIL_VERIFICATION_CODE_TTL     = 15 * 60 * 1000 ; // 15 minutes
config.EMAIL_VERIFICATION_CODE_LENGTH  = 6 ;

// Forgot Password verification
config.PASSWORD_VERIFICATION_MAX_ATTEMPTS = 5 ;
config.PASSWORD_VERIFICATION_CODE_TTL     = 15 * 60 * 1000 ; // 15 minutes
config.PASSWORD_VERIFICATION_CODE_LENGTH  = 6 ;

config.SAVEPOINT_ADMIN_EMAIL = argv.savepoint_admin_email || 'info@savepoint.party' ;

// Postgres
config.PG_HOST            = env.PG_HOST     || argv.pg_host     || 'postgres'   
config.PG_PORT            = env.PG_PORT     || argv.pg_port     || 5432
config.PG_DATABASE        = env.PG_DATEBASE || argv.pg_database || 'spdb'
config.PG_USER            = env.PG_USER     || argv.pg_user     || 'savepoint'
config.PG_PASSWORD        = env.PG_PASSWORD || argv.pg_password || 'savepoint'
config.PG_POOLSIZE        = env.PG_POOLSIZE || argv.pg_poolsize || 10

// Facebook Integration
config.FACEBOOK_SECRET = '11787024efd50d0fe888803686abaa58' ; // I know this is a horrible place to put this, but fuck you instead.
config.FACEBOOK_APP_ID = '1529152384073651' 

// Stripe / Braintree payments token
config.STRIPE_API_TOKEN = 'sk_test_8rGk8PlRJg4Zc3Zl4L0Tr69Q' 

// Sendgrid
config.SENDGRID_API_KEY = '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'


// Messages
config.MESSAGE_PAGE_SIZE = 20 

// etc...
module.exports = config 
