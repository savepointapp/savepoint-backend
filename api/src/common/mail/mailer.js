const argv   = require('minimist')(process.argv.slice(2))
const client = require('../clients/sendgrid.js')
const config = require('../config.js')

const mailer = {} ;

mailer.wrapped_send = async function(options){
    if (argv.production || argv.allow_emails){
        client.send(options)
    } else {
        console.log(`non-production env -- don't send email: ${JSON.stringify(options,undefined,2)}`)
    }
}

mailer.send_email_verification_code = async function({ email, code}){
    return mailer.wrapped_send({
        to: email,
        from: config.SAVEPOINT_ADMIN_EMAIL,
        subject: "Verify your email",
        text: `Your code is ${code}`,
        // html: "<p> html </p>",
    }).catch(err => {
        console.log( JSON.stringify(err.response, undefined, 2) )
        throw err
    })
}


mailer.send_matched_request = async function( email ){
    return mailer.wrapped_send({
        to: email,
        from: config.SAVEPOINT_ADMIN_EMAIL,
        subject: "Your event was matched!",
        text: 'Your event was matched! You have two days to talk with your new potential roommates. Hope you make some new friends!',
        // html: "<p> html </p>",
    }).catch(err => {
        console.log( JSON.stringify(err.response, undefined, 2) )
        throw err
    })
}

mailer.send_unmatched_request = async function( email ){
    return mailer.wrapped_send({
        to: email,
        from: config.SAVEPOINT_ADMIN_EMAIL,
        subject: "Your group was disbanded",
        text: "Hey, Sorry to have to tell you this. Your group disbanded. Don't worry though, We're on the case!",
        // html: "<p> html </p>",
    }).catch(err => {
        console.log( JSON.stringify(err.response, undefined, 2) )
        throw err
    })
}

mailer.send_reset_password_code = async function({ email, code}){
    return mailer.wrapped_send({
        to: email,
        from: config.SAVEPOINT_ADMIN_EMAIL,
        subject: "Password Reset: Was this you?",
        text: `Your code is ${code}`,
        // html: "<p> html </p>",
    }).catch(err => {
        console.log( JSON.stringify(err.response, undefined, 2) )
        throw err
    })
}


module.exports = mailer ;


// const msg = {
//   to: 'test@example.com',
//   from: 'test@example.com',
//   subject: 'Sending with SendGrid is Fun',
//   text: 'and easy to do anywhere, even with Node.js',
//   html: '<strong>and easy to do anywhere, even with Node.js</strong>',
// };