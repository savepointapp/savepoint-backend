const pg     = require('../../common/clients/pg.js').getClient()

const savepoint_requests = {}

const BASE_QUERY = `

        select sp.id              as id,
               sp.user_id         as user_id,
               sp.event_id        as event_id,
               row_to_json(r.*)   as room, 
               row_to_json(rp.*)  as room_prefs,
               row_to_json(gp.*)  as prefs,
               row_to_json(gr.*)   as group

          from savepoint_requests as sp 

          left outer join rooms as r
            on sp.room_id = r.id

          left outer join room_preferences as rp
            on sp.room_preferences_id = rp.id

          left outer join general_preferences as gp
            on sp.general_preferences_id = gp.id

          left outer join (
              select g.id             as id, 
                     row_to_json(r.*) as room
                from groups as g
                left outer join rooms as r
                  on g.room_id = r.id
          ) as gr  
            on sp.group_id = gr.id`

const FILTER_BY_USER_ID  = `where sp.user_id = $<user_id>`
const FILTER_BY_EVENT_ID = `where sp.event_id = $<event_id>`
const FILTER_BY_ID       = `where sp.id = $<id>`
const FILTER_BY_ROOM_ID  = `where sp.room_id = $<room_id>`
const AND_IS_NOT_DELETED = `and sp.deleted_on IS NULL`

savepoint_requests.list_for_user_id = user_id => pg.query(`
    ${BASE_QUERY} 
    ${FILTER_BY_USER_ID}
    ${AND_IS_NOT_DELETED}`
, {user_id})

savepoint_requests.list_for_event_id = user_id => pg.query(`
    ${BASE_QUERY} 
    ${FILTER_BY_EVENT_ID}
    ${AND_IS_NOT_DELETED}`
, {event_id})

savepoint_requests.fetch = id => pg.one(`
    ${BASE_QUERY} 
    ${FILTER_BY_ID}
    ${AND_IS_NOT_DELETED}`     
, {id})

savepoint_requests.fetch_raw_by_room_id = room_id => pg.one(`
    select * 
      from savepoint_requests as sp
     where sp.room_id = $<room_id>
    ${AND_IS_NOT_DELETED}`
, {room_id})


savepoint_requests.create = function(data, t) {
    return (t||pg).one(`
        INSERT INTO savepoint_requests (
            event_id
        ,   user_id
        ,   room_id
        ,   room_preferences_id
        ,   general_preferences_id
        ) VALUES (
            $<event_id>
        ,   $<user_id>
        ,   $<room_id>
        ,   $<room_preferences_id>
        ,   $<general_preferences_id>
        )
        RETURNING id`
    , data )
}

savepoint_requests.set_group = function(data, t) {
    return (t||pg).query(`
        UPDATE savepoint_requests as sp
           SET group_id = $<group_id>
         WHERE sp.id = $<request_id>
         ${AND_IS_NOT_DELETED}`
    , data)
}

savepoint_requests.kick_member = function(data, t) {
    return (t||pg).query(`
        UPDATE savepoint_requests as sp
           SET group_id = NULL
         WHERE group_id = $<group_id>
           AND user_id = $<user_id>`
    , data)
}

savepoint_requests.disband_group = function(data, t){
    return (t||pg).query(`
        UPDATE savepoint_requests as sp
           SET group_id = NULL
         WHERE group_id = $<group_id>`
    , data)
}

savepoint_requests.delete = request_id => pg.query(`
    update savepoint_requests  
       set deleted_on = NOW()
     where id = $<request_id>`
, {request_id})

savepoint_requests.all_requests = function(){
    return pg.query(BASE_QUERY, null)
}

module.exports = savepoint_requests

