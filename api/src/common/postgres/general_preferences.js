const pg     = require('../../common/clients/pg.js').getClient()

const general_preferences = {}

general_preferences.create = function(data, t){

    return (t||pg).one(`
        INSERT INTO general_preferences (
            number_of_roommates  
        ,   schedule             
        ,   noise_level          
        ,   gender_preference    
        ,   age_restriction
        ,   lgbtq_friendly      
        ) VALUES (
            $<number_of_roommates>  
        ,   $<schedule>             
        ,   $<noise_level>          
        ,   $<gender_preference>    
        ,   $<age_restriction> 
        ,   $<lgbtq_friendly>        
        )
        RETURNING id`
    , data )

}

module.exports = general_preferences