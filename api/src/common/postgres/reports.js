const pg     = require('../../common/clients/pg.js').getClient()

const reports = {}

// Query builder
const SELECT_QUERY = `
    select *
      from reports
`
const FILTER_BY_USER_ID  = `where user_id = $<user_id>`
const FILTER_BY_GROUP_ID = `where group_id = $<group_id>`

reports.create = function(data){
    return pg.one(`
        insert into reports (
            user_id,
            other_user_id,
            group_id,
            reason
        ) values (
            $<user_id>,
            $<other_user_id>,
            $<group_id>,
            $<reason>
        )
        returning *`
    , data )
}




reports.select = function({
    user_id,
    other_user_id, // NOT RESPECTED TODO RESPECT MEEE
    group_id
}){
    return user_id
         ? pg.query(SELECT_QUERY + FILTER_BY_USER_ID  , {user_id})
         : pg.query(SELECT_QUERY + FILTER_BY_GROUP_ID , {group_id})
}

module.exports = reports


