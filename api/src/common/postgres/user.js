const pg = require('../clients/pg.js').getClient()

const user = {}

// -----------------------------------
//  Fetch user info for a social account 
// -----------------------------------

user.lookup_from_social_id = (social_id, provider) => pg.one(`
    SELECT u.*
      FROM social_user AS s
      JOIN users AS u
        ON s.user_id = u.id
     WHERE provider = $<provider>
       AND social_id = $<social_id>`
, { 
    social_id,
    provider
})

// TODO Reimplement this so its not shitty
user.lookup_by_id    = id => pg.one(`select * from users where id = $<id>`, {id})
user.lookup_by_email = email => pg.one(`select * from users where email = $<email>`, {email})

// -----------------------------------
//  Create a new user account 
// -----------------------------------

/*
{
    username,
    encrypted_password,
    email,
    bio,
    first_name,
    last_name,
    email_sub,
    age,
    tos,
    gender
}
*/
user.create = function({
    username,
    encrypted_password,
    email,
    bio,
    first_name,
    last_name,
    email_sub,
    image_url,
    birthday,
    tos,
    gender,
    sex,
    created_on,
}, t){
    return (t || pg).one(`
        INSERT INTO users (
            username,
            encrypted_password,
            email,
            bio,
            first_name,
            last_name,
            email_sub,
            image_url,
            birthday,
            tos,
            gender,
            sex,
            created_on
        ) VALUES (
            $<username>,
            $<encrypted_password>,
            $<email>,
            $<bio>,
            $<first_name>,
            $<last_name>,
            $<email_sub>,
            $<image_url>,
            $<birthday>,
            $<tos>,
            $<gender>,
            $<sex>,
            NOW()
        ) RETURNING *`
    , {
        username,
        encrypted_password,
        email,
        bio,
        first_name,
        last_name,
        email_sub ,
        birthday,
        tos,
        gender,
        sex,
        created_on,
        image_url
    })
}

user.update = function({
    id,
    first_name,
    last_name,
    bio,
    email,
    is_email_verified,
    encrypted_password,
    email_sub,
    birthday,
    tos,
    gender,
    sex,
    image_url
}){
    return pg.one(`
        UPDATE users
           SET bio                = COALESCE($<bio>                 , users.bio)
             , first_name         = COALESCE($<first_name>          , users.first_name)
             , last_name          = COALESCE($<last_name>           , users.last_name)
             , email              = COALESCE($<email>               , users.email)
             , is_email_verified  = COALESCE($<is_email_verified>   , users.is_email_verified)
             , birthday                = COALESCE($<birthday>                 , users.birthday)
             , gender             = COALESCE($<gender>              , users.gender)
             , sex                = COALESCE($<sex>                 , users.sex)
             , email_sub          = COALESCE($<email_sub>           , users.email_sub)
             , tos                = COALESCE($<tos>                 , users.tos)
             , encrypted_password = COALESCE($<encrypted_password>  , users.encrypted_password)
             , image_url          = COALESCE($<image_url>           , users.image_url)
         WHERE id = $<id>
         RETURNING *`
    , {
        id,
        first_name,
        last_name,
        bio,
        email,
        is_email_verified,
        encrypted_password,
        email_sub,
        birthday,
        tos,
        gender,
        sex,
        image_url
    })

}
// -----------------------------------
//  Create a new social account
// -----------------------------------


user.create_social_account  = ({social_id, provider, user_id}) => pg.query(`
    INSERT INTO social_user (
        provider,
        social_id,
        user_id,
        created_on,
        updated_on
    ) VALUES (
        $<provider>,
        $<social_id>,
        $<user_id>,
        NOW(),
        NOW()
    )`
, {
    provider,
    social_id,
    user_id
})

module.exports = user