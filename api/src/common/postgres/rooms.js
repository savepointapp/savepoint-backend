
const pg     = require('../../common/clients/pg.js').getClient()

const rooms = {}

rooms.create = function(data, t){
    return (t||pg).one(`
        INSERT INTO rooms (
            check_in_date        
        ,   check_out_date       
        ,   price_total          
        ,   accommodation_name   
        ,   address_line_1       
        ,   address_line_2       
        ,   zipcode              
        ,   city                 
        ,   state         
        ,   other_details               
        ) VALUES (
            $<check_in_date>         
        ,   $<check_out_date>        
        ,   $<price_total>           
        ,   $<accommodation_name>    
        ,   $<address_line_1>        
        ,   $<address_line_2>        
        ,   $<zipcode>               
        ,   $<city>                  
        ,   $<state>             
        ,   $<other_details>            
        ) 
        RETURNING id`
    , data)
}

rooms.update = function({
    id        
,   check_in_date        
,   check_out_date       
,   price_total          
,   accommodation_name   
,   address_line_1       
,   address_line_2       
,   zipcode              
,   city                 
,   state         
,   other_details  
}){
    return pg.one(`
        UPDATE rooms
           SET check_in_date      = COALESCE($<check_in_date>      , check_in_date)           
             , check_out_date     = COALESCE($<check_out_date>     , check_out_date)          
             , price_total        = COALESCE($<price_total>        , price_total)             
             , accommodation_name = COALESCE($<accommodation_name> , accommodation_name)      
             , address_line_1     = COALESCE($<address_line_1>      , address_line_1)          
             , address_line_2     = COALESCE($<address_line_2>     , address_line_2)         
             , zipcode            = COALESCE($<zipcode>            , zipcode)                 
             , city               = COALESCE($<city>               , city)                    
             , state              = COALESCE($<state>              , state)                   
             , other_details      = COALESCE($<other_details>      , other_details)
         WHERE id = $<id>
        RETURNING *`
    , {
        id
    ,   check_in_date        
    ,   check_out_date       
    ,   price_total          
    ,   accommodation_name   
    ,   address_line_1       
    ,   address_line_2       
    ,   zipcode              
    ,   city                 
    ,   state         
    ,   other_details  
    })
}

rooms.get = function(
    room_id, /* :number */
)/* :RoomObject */ {
    return pg.one(`
        SELECT * 
          FROM rooms
         WHERE id = $<room_id>`
    , {room_id})
}

rooms.owned_by = function(
    user_id, /* :number */
    room_id, /* :number */
) /* :Promise<boolean> */ {
    return pg.one(`
        SELECT count(*)
          FROM savepoint_requests as sp
         WHERE sp.user_id = $<user_id>
           AND sp.room_id = $<room_id>`
    , {user_id, room_id})
    .then(res => res.count === 1)
}

module.exports = rooms ;