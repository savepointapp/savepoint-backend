const pg     = require('../../common/clients/pg.js').getClient()

const room_preferences = {} 

room_preferences.create = function(data, t){
    return (t||pg).one(`
        INSERT INTO room_preferences (
            check_in_date        
        ,   check_out_date   
        ,   max_budget
        ,   other_details        
        ) VALUES (
            $<check_in_date>        
        ,   $<check_out_date>  
        ,   $<max_budget>
        ,   $<other_details>        
        )
        RETURNING id`
    , data )
}

module.exports = room_preferences