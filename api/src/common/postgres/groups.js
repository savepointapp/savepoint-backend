const pg     = require('../../common/clients/pg.js').getClient()

const groups = {}


groups.contains_member = function(data){

    return pg.one(`
        select count(*)
          from savepoint_requests as sp
         where group_id = $<group_id>
           and user_id = $<user_id>
           and sp.deleted_on IS NULL`
    , data ).then(row => row.count == 1 )


}

groups.fetch_by_id_v2 = function(id){
    return pg.one(`
        with i as (
            select  row_to_json(t) from (
                select  sp.room_id IS NOT NULL as is_leader,        
                        row_to_json(gp.*) as prefs,
                        (   
                            select  row_to_json(t2) from (
                                select    id
                                        , email
                                        , email_sub  
                                        , first_name 
                                        , last_name  
                                        , bio        
                                        , birthday        
                                        , gender
                                        , sex
                                        , image_url 
                                from  users where users.id = u.id
                            ) t2
                        ) as member
                  from savepoint_requests as sp
                  join users as u
                    on sp.user_id = u.id
                  join general_preferences as gp
                    on sp.general_preferences_id = gp.id
                 where sp.group_id = $<id>
                   and sp.deleted_on IS NULL
            ) t
       )
        select  g.id ,

                row_to_json(r.*) as room,
                (select array_to_json(array_agg(row_to_json(i.*))) from i) as members

            from groups as g
            join rooms as r
              on g.room_id = r.id
            where g.id = $<id> ;`
    , { id })
}

groups.create = function(data, t){

    return (t||pg).one(`
        INSERT INTO groups (
            room_id    
        ) VALUES (
            $<room_id>
        )
        RETURNING id`
    , data )

}

groups.is_owner = function(data){
    return pg.one(`
        select count(*)
          from savepoint_requests 
         where user_id = $<user_id>
           and group_id = $<group_id>
           and room_id is not null`
    , data)
}
module.exports = groups
