const pg = require('../clients/pg.js').getClient()

const events = {} 

// ----------------------------------
// Create and Event -- should be admin only
// ----------------------------------

events.create = function({
    name,
    description,
    image_url,
    start_date,
    end_date,
    city,
    state
}){
    return pg.one(`
        INSERT INTO events (
            name,
            description,
            image_url,
            start_date,
            end_date,
            city,
            state,
            created_on,
            updated_on
        ) VALUES (
            $<name>,
            $<description>,
            $<image_url>,
            $<start_date>,
            $<end_date>,
            $<city>,
            $<state>,
            NOW(),
            NOW()
        ) RETURNING *`
        , {
            name,
            description,
            image_url,
            start_date,
            end_date,
            city,
            state,
    })
}


// ----------------------------------
// List all events
// ----------------------------------

events.all = () => pg.query(`SELECT * FROM events`)

events.fetch_by_id = function(id){
    return pg.query(`
    SELECT * 
      FROM events
     WHERE events.id = $<id>
    `,{ id })
}

events.search = function({
    start_date,
    end_date,
    limit,
    offset
}){
    return pg.query(`
        SELECT *
          FROM events
         WHERE start_date > $<start_date>::date
           AND end_date < $<end_date>::date
         LIMIT $<limit>
         OFFSET $<offset>`
    , {
        start_date,
        end_date,
        limit,
        offset
    })

}

events.update = function({
    id,
    name,
    description,
    image_url,
    start_date,
    end_date,
    city,
    state
}){
    return pg.query(`
    UPDATE events
        SET name = $<name>,
            description = $<description>,
            image_url = $<image_url>,
            start_date = $<start_date>,
            end_date = $<end_date>,
            city = $<city>,
            state = $<state>
        WHERE id = $<id>`
    , {
        id,
        name,
        description,
        image_url,
        start_date,
        end_date,
        city,
        state
    })
}

module.exports = events