
const pg     = require('../../common/clients/pg.js').getClient()

const messages = {}

messages.filter = function({
    group_id,
    limit,
    offset,
}){
    return pg.query(`
        SELECT * 
          FROM messages
         WHERE group_id = $<group_id>
         LIMIT $<limit>
        OFFSET $<offset>`
    , {
        group_id,
        limit,
        offset
    })
}

module.exports = messages ;