const pg = require('../clients/pg.js').getClient()

const verification_codes = {}

verification_codes.lookup = function({user_id, code}){
    return pg.one(`
        select *
          from verification_codes
         where user_id = $<user_id>
           and code = $<code>`
    , { user_id,
        code
    })
}

// What is this even for ?
// verification_codes.select = ({user_id, email}, t) => (t || pg).query(`
//     SELECT *
//       FROM verification_codes
//      WHERE email   = $<email>
//         OR user_id = $<user_id>`
// , {email, user_id})

verification_codes.record_failed_attempt = ({user_id, email}) => pg.query(`
    UPDATE verification_codes
       SET attempts = attempts + 1
     WHERE email   = $<email>
        OR user_id = $<user_id>`
, {email, user_id})

verification_codes.set_used = ({user_id, code}) => pg.query(`
    UPDATE verification_codes
       SET is_used = true
     WHERE code = $<code>
       AND user_id = $<user_id>`
, {user_id, code})

verification_codes.create = function({ user_id, email, code }, t){
    return (t || pg).query(`
    INSERT INTO verification_codes (
        user_id,
        email,
        code
    ) VALUES (
        $<user_id>,
        $<email>,
        $<code>
    )
    RETURNING *`, {
        user_id,
        email,
        code
    })
}

module.exports = verification_codes
