const pg = require('../clients/pg.js').getClient()

const codes = {} 

codes.insert = function({ code, user_id, op }) {
    return pg.one(`INSERT INTO codes (
        code,
        user_id,
        op
    ) VALUES (
        $<code>,
        $<user_id>,
        $<op>
    )
    RETURNING *`
    , {
        code,
        user_id,
        op
    })
}

codes.from_code = code => pg.one(`SELECT * FROM codes WHERE code = $<code>`, {code})

module.exports = codes