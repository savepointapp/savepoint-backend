module.exports = {
    codes:                       require('./codes.js')               ,
    events:                      require('./events.js')              ,
    general_preferences:         require('./general_preferences.js') ,
    groups:                      require('./groups.js')              ,
    messages:                    require('./messages.js')            ,
    reports:                     require('./reports.js')             ,
    room_preferences:            require('./room_preferences.js')    ,
    rooms:                       require('./rooms.js')               ,
    savepoint_requests:          require('./savepoint_requests.js')  ,
    user:                        require('./user.js')                ,
    verification_codes:          require('./verification_codes.js')  ,   
}