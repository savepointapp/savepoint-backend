'use strict'
const argv = require('minimist')(process.argv.slice(2))
const Bluebird = require('bluebird')
// Allow access to *Async methods
Bluebird.promisifyAll(require('request'))

let config = {} ;

if (argv.local){
    config = require('./local_config.js')
} else if (argv.production){
    config = require('./prod_config.js')
} else {
    console.log("oops! must run with --local or --production!")
    process.exit(1)
}

Object.freeze(config) 
module.exports = config 