const client = require('@sendgrid/mail');
const config = require('../config.js')
client.setApiKey(config.SENDGRID_API_KEY);
console.log(`Using ${config.SENDGRID_API_KEY}`)

module.exports = client

// Example message format
// const msg = {
//   to: 'test@example.com',
//   from: 'test@example.com',
//   subject: 'Sending with SendGrid is Fun',
//   text: 'and easy to do anywhere, even with Node.js',
//   html: '<strong>and easy to do anywhere, even with Node.js</strong>',
// };
