const Bluebird = require('bluebird')
const request = require('request')
const config  = require('../config.js')
const SavepointError = require('../errors/SavepointError.js')

const fb = {} ;

// Get Savepoint app access token.
// Further reading: https://developers.facebook.com/docs/facebook-login/access-tokens#apptokens
// I tried to do this the right way but it didnt work so we're doing it this way for now .
fb.get_app_access_token = () => request.getAsync(`https://graph.facebook.com/v2.12/oauth/access_token?client_id=${config.FACEBOOK_APP_ID}&client_secret=${config.FACEBOOK_SECRET}&grant_type=client_credentials`)
.then(res => {
    if (res.statusCode == 200){
        return JSON.parse(res.body).access_token
    }
    console.log('Something bad happened while signing up with fb')
    throw new Error('Something went wrong!')    
})


// Inspect (validate) a token and identify the associated fb user id
fb.inspect_token = (token, app_access_token) => request.getAsync({
    url: 'https://graph.facebook.com/debug_token',
    qs: {
        input_token: token,
        access_token: app_access_token
    }
}) 

fb.make_curl_string = token => console.log(`https://developers.facebook.com/tools/debug/accesstoken/?access_token=${token}&version=v2.12`)

fb.verify = async function(token){
    console.log(`Verifying facebook token:  ${token}.`)
    const app_access_token = await fb.get_app_access_token() ;
    const res = await fb.inspect_token(token, app_access_token)
    const data = JSON.parse(res.body).data
    // console.log(data)
    if (!data.is_valid || data.app_id !== config.FACEBOOK_APP_ID){
        throw new SavepointError({ code: 400, message: 'Invalid access token.' })
    } 
    return data.user_id
}

module.exports = fb

//  fb.verify('EAAVuwYioZC7MBAPgaO2NZCZAIiYglWMzWBbREcCk5ZAuCE1QgGgHnbDBdPFCzMP3PzgZBJsoZCBVN1b1wdRl1eioUr8bZCRZA1EKd75H0eNv2yuubXWdAFkBCRm8oRcPYNFJZAwAbPUhwIQ6T8ZBQfzsVEZCCL83bsWcpaiu6D10Xst4mpBFu0L8P3lG4DP6Cac9UYZD')