'use strict'
const Bluebird = require('bluebird')
const pgp      = require('pg-promise')( { promiseLib: Bluebird })
const config   = require('../config.js')

class PostgresClient {

    constructor(_config) {
        this._config = _config ;
        this.pg = pgp(_config) ;
        this.outstanding_connections = 0 ;
    }
    
    getConnection () {
        let connection ; 
        return Bluebird.resolve(this.pg.connect())
        .then( conn => {
            this.outstanding_connections += 1
            connection = conn ;
            return conn
        })
        .disposer( () => {
            this.outstanding_connections -= 1
            connection.done()
        })
    }

    query (query_string, data) {
        return Bluebird.using( this.getConnection(), conn => conn.query(query_string, data))
    }

    one (query_string, data) {
        return Bluebird.using( this.getConnection(), conn => conn.one(query_string, data))
    }

    none (query_string, data) {
        return Bluebird.using( this.getConnection(), conn => conn.none(query_string, data))
    }

    tx (txfn) {
        return Bluebird.using( this.getConnection(), conn => conn.tx(txfn))
    }

}

const client = new PostgresClient({
    host     : config.PG_HOST,
    port     : config.PG_PORT,
    database : config.PG_DATABASE,
    user     : config.PG_USER,
    password : config.PG_PASSWORD,
    poolSize : config.PG_POOLSIZE
})

module.exports = {
    getClient: () => client,
    PostgresClient
}