'use strict'
module.exports = class SavepointError extends Error {
    constructor({ code, message }){
        super(message)
        this.code = code 
    }
}