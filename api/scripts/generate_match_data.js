const postgres = require('../src/common/postgres/tables.js')
const SPClient = require('../test/clients/SPClient.js')
const  bluebird = require('bluebird')
const _ = require('lodash')

const EVENT_JSONS = [

    {
        name:"GOOD EVENT",
        description: "THIS will be a very good event. You should come to this event, it will be a blast. I need more text to [pad this but I dont want to use lorem ipsum since I would have to look that up and Im too lazy to access the internet right now.",
        image_url: "http://cdn1-www.dogtime.com/assets/uploads/gallery/cardigan-welsh-corgi/olderderp-6_680-453.jpg",
        start_date: "2018-06-30",
        end_date: "2018-07-03",
        city: "Boston",
        state: "MA"
    },

    {
        name:"GOOD EVENT #2 ",
        description: "THIS will be a much much better event than good event. You should come to this event, it will be a bigger blast. Lorem ipsum fuck you",
        image_url: "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12225921/Pembroke-Welsh-Corgi-MP.jpg",
        start_date: "2018-07-13",
        end_date: "2018-07-15",
        city: "Boston",
        state: "MA"
    },

    {
        name:"GOOD EVENT #3 ",
        description: "This is just a corgi.",
        image_url: "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12225921/Pembroke-Welsh-Corgi-MP.jpg",
        start_date: "2018-07-17",
        end_date: "2018-07-21",
        city: "Boston",
        state: "MA"
    }

]

// See general_preferences schema
const prefs = {
    number_of_roommates  : 1,
    schedule             : 'night_owl' ,
    noise_level          : 'social' ,
    gender_preference    : 'coed' ,
    age_restriction      : false  ,
    lgbtq_friendly       : true,
} 

const room_prefs = {
    check_in_date        : new Date('3000-01-01') , //.valueOf() ,
    check_out_date       : new Date('3000-01-04') , //.valueOf() ,
    max_budget           : 200,
    other_details        : "Pikachu is great!"
} 

const room = {
    check_in_date                 : new Date('3000-01-01') , //.valueOf() ,
    check_out_date                : new Date('3000-01-04') , //.valueOf() ,
    price_total                   : 800 ,
    accommodation_name            : 'Some Hotel' ,
    address_line_1                : '123 Address street' ,
    address_line_2                : 'Room 1408' ,
    zipcode                       : '12345' ,
    city                          : 'San Francisco' ,
    state                         : 'CA' ,
    other_details                 : "Join us if you love pokemon!"
}

const make_request_params = function(event_id, has_room){
    const req = { event_id, prefs }
    return has_room ? {...req,room} : {...req,room_prefs}
}

// Run this to dump it.

const run = async function(){

    // Make Users
    const clients = await bluebird.map(_.range(6), async i => {
        console.log(i);
        const cli = new SPClient('localhost:8000')
        await cli.reg_and_verify({
            body: {
                tos      : true,
                email    : `user${i}@savepoint.party`,
                password : `Password${i}!`,
                first_name: `Savepoint`,
                last_name: `User${i}`,
                birthday: 682646400000 + i,
                gender: (['male','female', 'non_binary'])[i%3],
                sex: (['male', 'female', 'prefer_not_to_say'])[i%3]
            } 
        })
        return cli
    })

    // Actually create the events
    bluebird.map(EVENT_JSONS, async e => {
        const data = await postgres.events.create(e).catch (err => console.log(err))
    })
    
    console.log(clients);

    // Make admin user
    const admin = clients[5]
    await admin.set_admin()        

    // Make requests:
    console.log('making grouped reqs')
    const grouped_requests = await bluebird.all([

        bluebird.all([
            // Group 1
            clients[0].new_request({ body:  make_request_params( 1, true ) }),
            clients[1].new_request({ body:  make_request_params( 1, false) }),
            clients[2].new_request({ body:  make_request_params( 1, false) }),
            clients[3].new_request({ body:  make_request_params( 1, false) }),
        ]),

        bluebird.all([
            // Group 2
            clients[0].new_request({ body:  make_request_params( 2, false) }),
            clients[1].new_request({ body:  make_request_params( 2, true ) }),
            clients[3].new_request({ body:  make_request_params( 2, false) }),
            clients[4].new_request({ body:  make_request_params( 2, false) }),
        ]),

        bluebird.all([
            // Group 3
            clients[0].new_request({ body:  make_request_params( 3, false) }),
            clients[1].new_request({ body:  make_request_params( 3, true ) }),
            clients[2].new_request({ body:  make_request_params( 3, false) }),
            clients[4].new_request({ body:  make_request_params( 3, false) }),
        ]),

    ])

    // Admin matches groups
    console.log('match groups together');
    await admin.make_group({ body: { request_ids: _.map(grouped_requests[0], 'body.id') } }) ;
    await admin.make_group({ body: { request_ids: _.map(grouped_requests[1], 'body.id') } }) ;
    // await admin.make_group({ body: { request_ids: _.map(grouped_requests[2], 'body.id') } }) ;
    console.log('done')
}

run()

// Run this to dump it.
// PGPASSWORD=password pg_dump --data-only spdb -U postgres -h localhost -F c > wtf.sql 
// PGPASSWORD=password pg_restore -d spdb -U postgres -h localhost < wtf.sql 