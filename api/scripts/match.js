const uuid = require('uuid')
const SPClient = require('../test/clients/SPClient.js')
const  bluebird = require('bluebird')
const _ = require('lodash')
const argv = require('minimist')(process.argv.slice(2))

// Run this to dump it.

const run = async function(){

    const cli = new SPClient('18.222.208.127:8000')
    const x = uuid.v1() // lol wtffff
    await cli.reg_and_verify({
        body: {
            tos      : true,
            email    : `user${x}@savepoint.party`,
            password : `password${x}`,
            first_name: `Savepoint`,
            last_name: `User${x}`,
            birthday: 682646400000,
            gender: 'male',
            sex: 'male'
        } 
    })
    
    // Make admin user
    const admin = cli
    await admin.set_admin()     

    const request_ids = argv.request_ids.split(',')

    // Admin matches groups
    await admin.make_group({ body: { request_ids } }) ;

}

run()
