#!/bin/bash
export host=$1
pushd scripts
. init.sh        $host
. migrate.sh     $host
. import_data.sh $host
popd