#!/bin/bash

HOST=$1
USER='postgres'
PASS='password'
DATABASE='spdb'
DROP_SCRIPT='./drop.sql'

PGPASSWORD=$PASS psql -U $USER -h $HOST < $DROP_SCRIPT