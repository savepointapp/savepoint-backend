#!/bin/bash

HOST=$1
USER='savepoint'
PASS='savepoint'
DATABASE='spdb'
DATA_DIR='../data'

for filename in $(ls $DATA_DIR | grep sql | sort -t- -k1 -k2 -k3); do
    printf "\n"
    echo "Data "./$DATA_DIR/$filename
    PGPASSWORD=$PASS pg_restore -U $USER -h $HOST -d $DATABASE < ./$DATA_DIR/$filename
done