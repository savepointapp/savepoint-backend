#!/bin/bash

HOST=$1
USER='savepoint'
PASS='savepoint'
DATABASE='spdb'
SQL_DIR='../sql'

for filename in $(ls $SQL_DIR | grep sql | sort -t- -k1 -k2 -k3); do
    printf "\n"
    echo "Migration "./$SQL_DIR/$filename
    PGPASSWORD=$PASS psql -U $USER -h $HOST $DATABASE < ./$SQL_DIR/$filename
done