#!/bin/bash
export host=$1
pushd scripts
. drop.sh		 $host
. init.sh        $host
. migrate.sh     $host
popd