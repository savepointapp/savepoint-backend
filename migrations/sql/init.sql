
CREATE SEQUENCE user_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1 ;
    
CREATE TYPE gender AS ENUM ('female', 'male', 'non_binary', 'self_describe' );
CREATE TYPE sex AS ENUM ('female', 'male', 'prefer_not_to_say');

CREATE TABLE users (
  id                 INTEGER DEFAULT NEXTVAL('user_seq'::regclass) NOT NULL ,
  username           TEXT,
  email              citext,
  is_email_verified  BOOLEAN DEFAULT FALSE,
  encrypted_password TEXT, -- Should probably limit the storage size 

  -- Profile info
  profile_pic_url    TEXT ,
  bio                TEXT ,
  gender             TEXT ,
  sex                sex,
  first_name         TEXT ,
  last_name          TEXT ,
  email_sub          BOOLEAN DEFAULT TRUE ,
  birthday           BIGINT ,
  tos                BOOLEAN ,
  image_url          TEXT, -- This needs to be replaced with a cdn key
  
  is_superuser       BOOLEAN NOT NULL DEFAULT false ,
  
  created_on         TIMESTAMP WITH TIME ZONE ,
  updated_on         TIMESTAMP WITH TIME ZONE ,

  UNIQUE (email)
) ;

ALTER TABLE users ADD CONSTRAINT user_pkey PRIMARY KEY (id) ;

--------------------------------------
-- Facebook Users
-- -- Use this to look up users during auth flow.           
-- when we receive an auth token from the fornt end we will use it to 
-- look up the user on the back end                 
--------------------------------------

CREATE TABLE social_user (
  provider  TEXT NOT NULL , -- facebook, google, etc  
  social_id TEXT NOT NULL ,
  user_id   INTEGER NOT NULL,
  metadata  TEXT, -- json blob
  created_on TIMESTAMP WITH TIME ZONE,
  updated_on TIMESTAMP WITH TIME ZONE,
  UNIQUE (social_id),
  UNIQUE (provider, user_id)
) ;

CREATE TABLE verification_codes (
  id                 UUID DEFAULT uuid_generate_v1() PRIMARY KEY,
  email              citext ,
  code               TEXT NOT NULL  ,
  user_id            INTEGER REFERENCES users (id) ,
  attempts           INTEGER NOT NULL DEFAULT 0 ,
  is_used            BOOLEAN NOT NULL DEFAULT false , 
  created_on         TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW() ,
  updated_on         TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
) ;

CREATE UNIQUE INDEX verification_code_idx_on_code ON verification_codes(code) ;
CREATE INDEX verification_code_idx_on_email ON verification_codes(email) ;


CREATE TABLE password_verification_codes (
  user_id            INTEGER NOT NULL REFERENCES users (id),
  email              citext PRIMARY KEY,
  code               TEXT NOT NULL  ,
  encrypted_password TEXT NOT NULL  ,
  attempts           INTEGER NOT NULL DEFAULT 0,
  is_used            BOOLEAN NOT NULL DEFAULT false,
  created_on         TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  updated_on         TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
) ;

CREATE UNIQUE INDEX password_verification_code_idx_on_code ON password_verification_codes(code) ;

CREATE SEQUENCE events_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1 ;

CREATE TABLE events (
  id                 INTEGER DEFAULT NEXTVAL('events_seq'::regclass) PRIMARY KEY ,
  name               TEXT NOT NULL,
  description        TEXT,
  city               TEXT,
  state              TEXT, -- Im lazy and dont want to do an enum now, but fix this before a real release.
  image_url          TEXT,
  start_date         TIMESTAMP WITH TIME ZONE,
  end_date           TIMESTAMP WITH TIME ZONE,
  created_on         TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  updated_on         TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
) ;


-- ENUMS 
CREATE TYPE convention_schedule AS ENUM ( 'night_owl', 'early_riser' ) ;
CREATE TYPE gender_preference   AS ENUM ( 'male_only', 'female_only', 'coed', 'non_binary_only') ;
CREATE TYPE noise_level         AS ENUM ( 'quiet', 'social') ;

CREATE TABLE rooms (
    id                            UUID DEFAULT uuid_generate_v1() PRIMARY KEY,
    check_in_date                 TIMESTAMP WITH TIME ZONE NOT NULL ,
    check_out_date                TIMESTAMP WITH TIME ZONE NOT NULL ,
    price_total                   NUMERIC(9,2) NOT NULL ,
    accommodation_name            TEXT NOT NULL,
    address_line_1                TEXT NOT NULL ,
    address_line_2                TEXT NOT NULL ,
    zipcode                       VARCHAR(13) NOT NULL , -- look this up
    city                          TEXT NOT NULL ,
    state                         VARCHAR NOT NULL, -- ENUM This needs to be an enum
    other_details                 TEXT  ,
    created_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    updated_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
) ;

CREATE TABLE groups (
    id                            UUID DEFAULT uuid_generate_v1() PRIMARY KEY ,
    room_id                       UUID REFERENCES rooms (id) ,
    created_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    updated_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
) ;

CREATE TABLE user_group_memberships (
    id                            UUID DEFAULT uuid_generate_v1() PRIMARY KEY,
    user_id                       INTEGER REFERENCES users(id) ,
    group_id                      UUID REFERENCES groups(id) ,
    is_deleted                    BOOLEAN NOT NULL DEFAULT false ,
    created_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    updated_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
) ;

CREATE TABLE general_preferences (
    id                            UUID DEFAULT uuid_generate_v1() PRIMARY KEY,
    number_of_roommates           INTEGER ,
    schedule                      convention_schedule ,
    noise_level                   noise_level ,  
    gender_preference             gender_preference ,
    age_restriction               boolean NOT NULL ,
    lgbtq_friendly                boolean NOT NULL DEFAULT TRUE ,
    created_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    updated_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
) ;

CREATE TABLE room_preferences (
    id                            UUID DEFAULT uuid_generate_v1() PRIMARY KEY ,
    check_in_date                 TIMESTAMP WITH TIME ZONE NOT NULL ,
    check_out_date                TIMESTAMP WITH TIME ZONE NOT NULL ,
    max_budget                    NUMERIC (9,2) NOT NULL ,
    other_details                 TEXT  ,
    created_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    updated_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
) ;

-- This is the god schema where everything else is stuck to 
CREATE TABLE savepoint_requests (
    id                            UUID DEFAULT uuid_generate_v1() PRIMARY KEY ,
    event_id                      INTEGER REFERENCES events(id) ,
    user_id                       INTEGER REFERENCES users(id) ,
    room_id                       UUID REFERENCES rooms (id) ,
    room_preferences_id           UUID REFERENCES room_preferences (id) ,
    general_preferences_id        UUID REFERENCES general_preferences (id) ,
    group_id                      UUID REFERENCES groups (id),
    deleted_on                    TIMESTAMP WITH TIME ZONE
) ;

-- CREATE UNIQUE INDEX room_requests_idx_on_user_id ON room_requests(user_id) ;
-- CREATE UNIQUE INDEX room_requests_idx_on_event_id ON room_requests(event_id) ;


CREATE TABLE reports (
    id                            UUID DEFAULT uuid_generate_v1() PRIMARY KEY,
    user_id                       INTEGER REFERENCES users(id) NOT NULL,
    other_user_id                 INTEGER REFERENCES users(id) ,
    group_id                      UUID REFERENCES groups(id) ,
    reason                        TEXT NOT NULL, 
    created_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    updated_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
) ;

CREATE TABLE messages (
    id                            UUID DEFAULT uuid_generate_v1() PRIMARY KEY ,
    user_id                       INT REFERENCES users (id) ,
    group_id                      UUID REFERENCES groups (id) ,
    body                          TEXT NOT NULL, -- length should be verified in the app in case we ever want to change it.
    created_on                    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
) ;

CREATE INDEX messages_group_id_created_on ON messages(group_id, created_on) ;

CREATE TABLE codes (
  user_id            INTEGER NOT NULL REFERENCES users (id),
  code               TEXT NOT NULL  ,
  op                 TEXT NOT NULL ,   
  is_used            BOOLEAN NOT NULL DEFAULT false,
  created_on         TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  updated_on         TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
) ;

