// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"fmt"
	"bytes"
	"log"
	"net/http"
	"time"
	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
    hubManager *HubManager
	hub *Hub
	conn *websocket.Conn // The websocket connection.
	send chan []byte     // Buffered channel of outbound messages.
    GroupId string       // Has the user joined a chatroom yet?
    UserId int           // Has the user joined a chatroom yet?
}

type WsIncomingMessage struct {
    Action  string `json:"action"` // i.e. user is typing, user has read a message
    Message string  `json:"message"` // This is text send from the user for a plain message
    GroupId string  `json:"group_id"` 
    UserId  int     `json:"user_id"` 
}

type WsOutgoingMessage struct {
    Message string  `json:"message"` // This is text send from the user for a plain message
    GroupId string  `json:"group_id"` 
    UserId  int     `json:"user_id"` 
}

// Send GroupId to HubMaster
type HubRequest struct {
    channel  chan HubResponse
    GroupId string
}

// HubMaster should reply with a hub.
type HubResponse struct {
    hub *Hub
}

// Fetches the hub for a chat group and connects to it.
func (c *Client) connectToHub(GroupId string, UserId int) {
    channel := make(chan HubResponse)
    c.hubManager.channel <- HubRequest{ channel, GroupId }
    res := <- channel
    c.hub = res.hub
    c.GroupId = GroupId
    c.UserId = UserId
    c.hub.register <- c
}


// Since we have multiple different types of messages we 
// route them to different functionality within this function
func (c *Client) handleIncomingMessage(incoming WsIncomingMessage) {
    
    // fmt.Printf("Action: %s\n", incoming.Action)

    // The client must receive a "connect" action before any other messages
    // can be sent. This should check a users permissions and disconnect the
    // websocket after enough failed requests.
    if incoming.Action == "connect" {
        
        // Already connected!
        if c.hub != nil {
			fmt.Printf("[CONNECT] user_id=%v => group_id=%v (already connected)\n", incoming.UserId, incoming.GroupId)
            return
        }

        // Set the GroupId 
        if incoming.GroupId != "" {
            fmt.Printf("[CONNECT] user_id=%v => group_id=%v\n", incoming.UserId, incoming.GroupId)
            c.connectToHub(incoming.GroupId, incoming.UserId)
        } else {
			fmt.Printf("[CONNECT] user_id=%v => group_id=(none specified)", incoming.UserId)
		}
        return
    }

    // The client receives a message. Make sure they have joined a chat room already.
    if incoming.Action == "message" {

        // Broadcast message in this case
        if c.hub != nil && incoming.Message != "" {
            fmt.Printf("[MESSAGE] user_id=%v => group_id=%v\n", c.UserId, c.GroupId)

			// To storage
            SaveMessage(Message{ 
                user_id: c.UserId,
                group_id: c.GroupId,
                body: incoming.Message,
            })

			// To broadcast
			j, err := json.Marshal(WsOutgoingMessage{
				UserId  : c.UserId,
				GroupId : c.GroupId,
				Message : incoming.Message,
			}) 

			if err != nil {
				fmt.Printf("[MESSAGE] user_id=%v => group_id=%v OOPS SOMETHING BAD HAPPENED\n", incoming.UserId, incoming.GroupId)
				fmt.Println(err)
				return 
			}
            c.hub.broadcast <- []byte(string(j))
        }
        return
    }

} 

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {

        if c.hub != nil {
            c.hub.unregister <- c
        }
		
        if c.conn != nil {
            c.conn.Close()
        }
		
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {

		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
        var incoming WsIncomingMessage
        json.Unmarshal(message, &incoming)

		c.handleIncomingMessage(incoming)
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func serveWs(h *HubManager, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
    client := &Client{hubManager: h, conn: conn, send: make(chan []byte, 256)}

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}
