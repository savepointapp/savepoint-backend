package main

import (
    "database/sql"
    _ "github.com/lib/pq"
    "fmt"
)

const (
    DB_USER     = "savepoint"
    DB_PASSWORD = "savepoint"
    DB_NAME     = "spdb"
    DB_HOST     = "postgres"
    DB_PORT     = 5432
    DB_SSL_MODE = "disable"
)

type Message struct {
    id         string
    user_id    int    // user id 
    group_id   string // group id
    body       string // message body
    created_on string
}

func SaveMessage (m Message){

    connStr := fmt.Sprintf( "postgres://%s:%s@%s/%s?sslmode=%s", DB_USER, DB_PASSWORD, DB_HOST, DB_NAME, DB_SSL_MODE)
    db, err := sql.Open("postgres", connStr)
    checkErr(err)
    defer db.Close()
    // fmt.Printf("Trying to save (%d, %s, %s)\n", m.user_id, m.group_id, m.body)
    // var id int
    rows, err  := db.Query(`
        INSERT INTO messages (
            user_id,
            group_id,
            body
        ) VALUES (
            $1,
            $2,
            $3
        ) RETURNING id`,
        m.user_id,
        m.group_id,
        m.body,
    )
    checkErr(err)

    defer rows.Close()

}

func checkErr(err error) {
    if err != nil {
        panic(err)
    }
}