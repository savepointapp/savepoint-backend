package main 

import (
    // "encoding/json"
    "flag"
    "log"
	"fmt"
	"net/http"
)

var addr = flag.String("addr", ":8080", "http service address")

// Goals for today 2018-07-04:
//
// (*) Basic chat schemas 
// (*) Basic /ws/ endpoint on go server
// (*) Basic /chat/history/?index=6432
// (*)
//


/*
    How To Test:
    '{"group_id":"1","user_id":"1","what":"a string"}'
    curl localhost:8080/ws
    curl -XPOST -d '{"hello":1, "world": "fuck you"}' -H "Content-Type: application/json" localhost:8000/api/v1ping

    Test a websocket connection using cURL

    curl --include \
     -XPOST \
     -d '{}' \
     --no-buffer \
     --header "Connection: Upgrade" \
     --header "Upgrade: websocket" \
     --header "Host: example.com:80" \
     --header "Origin: http://example.com:80" \
     --header "Sec-WebSocket-Key: SGVsbG8sIHdvcmxkIQ==" \
     --header "Sec-WebSocket-Version: 13" \
     localhost:8080/ws


*/


// How to keep unwanted people from posting to a group chat? Each websocket is associated with a user and a group chat id.
// When the socket is opened we must validate (group_id, user_id) using application postgres.
// Once validated, we add the WsCli to the list of group members.

// When a message comes into a ws, we look at the associated group_id, save a message to postgres, and then send to the group. 

// var upgrader = websocket.Upgrader{
// 	ReadBufferSize:  1024,
// 	WriteBufferSize: 1024,
// }

// Initialize a websocket connection for a user, 
// subscribe them to the group.
type WSClientInit struct {
    group_id int
    user_id  int
    what     string
}

// Incoming chat messages/actions
type WSMessage struct {
    group_id int
    user_id  int
    message  string
    action   string
}

func main(){
    fmt.Println("Server running on 8080")
    flag.Parse()

    // Could make this a singleton
    // But dependency injection.
    h := newHubManager()
    go h.run()

    // Start webserver with one endpoint ws that can conditionally upgrade http connections to ws connections.
    http.HandleFunc("/ws", func( w http.ResponseWriter, r *http.Request) {
        fmt.Printf("%s %s\n", r.Method, r.RequestURI)
        serveWs(h, w, r)
    })

    err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}

