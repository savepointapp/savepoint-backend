package main

import (
    // "fmt"
)
// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type HubManager struct {
    channel    chan HubRequest
    hubMap     map[string]*Hub        // Map from GroupId => hub
}

func newHubManager() *HubManager {
	return &HubManager{
		channel : make(chan HubRequest),
        hubMap  : make(map[string]*Hub), 
    }
}

var hubManager = newHubManager()

// TODO support closing a hub when nobody is connected to it anymore.
func (h *HubManager) run() {
	for {
		message := <-h.channel
        // fmt.Printf("HubManager received message, found: %v\n", h.hubMap[message.GroupId])
        
        if h.hubMap[message.GroupId] == nil {
            // fmt.Printf("hubManager.run Found nil \n")
            hub := newHub()
            go hub.run()
            h.hubMap[message.GroupId] = hub
        }
        // fmt.Printf("HubManager Should be responding...\n")
        message.channel <- HubResponse{
            hub: h.hubMap[message.GroupId],
        } 	
	}
}
